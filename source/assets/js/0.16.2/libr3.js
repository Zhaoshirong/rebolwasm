var Module = typeof Module !== "undefined" ? Module : {};

var moduleOverrides = {};

var key;

for (key in Module) {
 if (Module.hasOwnProperty(key)) {
  moduleOverrides[key] = Module[key];
 }
}

var arguments_ = [];

var thisProgram = "./this.program";

var quit_ = function(status, toThrow) {
 throw toThrow;
};

var ENVIRONMENT_IS_WEB = false;

var ENVIRONMENT_IS_WORKER = false;

var ENVIRONMENT_IS_NODE = false;

var ENVIRONMENT_HAS_NODE = false;

var ENVIRONMENT_IS_SHELL = false;

ENVIRONMENT_IS_WEB = typeof window === "object";

ENVIRONMENT_IS_WORKER = typeof importScripts === "function";

ENVIRONMENT_HAS_NODE = typeof process === "object" && typeof process.versions === "object" && typeof process.versions.node === "string";

ENVIRONMENT_IS_NODE = ENVIRONMENT_HAS_NODE && !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_WORKER;

ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;

if (Module["ENVIRONMENT"]) {
 throw new Error("Module.ENVIRONMENT has been deprecated. To force the environment, use the ENVIRONMENT compile-time option (for example, -s ENVIRONMENT=web or -s ENVIRONMENT=node)");
}

var ENVIRONMENT_IS_PTHREAD = Module["ENVIRONMENT_IS_PTHREAD"] || false;

if (ENVIRONMENT_IS_PTHREAD) {
 buffer = Module["buffer"];
 DYNAMIC_BASE = Module["DYNAMIC_BASE"];
 DYNAMICTOP_PTR = Module["DYNAMICTOP_PTR"];
}

var _scriptDir = typeof document !== "undefined" && document.currentScript ? document.currentScript.src : undefined;

if (ENVIRONMENT_IS_NODE) {
 _scriptDir = __filename;
} else if (ENVIRONMENT_IS_WORKER) {
 _scriptDir = self.location.href;
}

var scriptDirectory = "";

function locateFile(path) {
 if (Module["locateFile"]) {
  return Module["locateFile"](path, scriptDirectory);
 }
 return scriptDirectory + path;
}

var read_, readAsync, readBinary, setWindowTitle;

if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
 if (ENVIRONMENT_IS_WORKER) {
  scriptDirectory = self.location.href;
 } else if (document.currentScript) {
  scriptDirectory = document.currentScript.src;
 }
 if (scriptDirectory.indexOf("blob:") !== 0) {
  scriptDirectory = scriptDirectory.substr(0, scriptDirectory.lastIndexOf("/") + 1);
 } else {
  scriptDirectory = "";
 }
 if (!(typeof window === "object" || typeof importScripts === "function")) throw new Error("not compiled for this environment (did you build to HTML and try to run it not on the web, or set ENVIRONMENT to something - like node - and run it someplace else - like on the web?)");
 if (ENVIRONMENT_HAS_NODE) {
  read_ = function shell_read(filename, binary) {
   if (!nodeFS) nodeFS = require("fs");
   if (!nodePath) nodePath = require("path");
   filename = nodePath["normalize"](filename);
   return nodeFS["readFileSync"](filename, binary ? null : "utf8");
  };
  readBinary = function readBinary(filename) {
   var ret = read_(filename, true);
   if (!ret.buffer) {
    ret = new Uint8Array(ret);
   }
   assert(ret.buffer);
   return ret;
  };
 } else {
  read_ = function shell_read(url) {
   var xhr = new XMLHttpRequest();
   xhr.open("GET", url, false);
   xhr.send(null);
   return xhr.responseText;
  };
  if (ENVIRONMENT_IS_WORKER) {
   readBinary = function readBinary(url) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, false);
    xhr.responseType = "arraybuffer";
    xhr.send(null);
    return new Uint8Array(xhr.response);
   };
  }
  readAsync = function readAsync(url, onload, onerror) {
   var xhr = new XMLHttpRequest();
   xhr.open("GET", url, true);
   xhr.responseType = "arraybuffer";
   xhr.onload = function xhr_onload() {
    if (xhr.status == 200 || xhr.status == 0 && xhr.response) {
     onload(xhr.response);
     return;
    }
    onerror();
   };
   xhr.onerror = onerror;
   xhr.send(null);
  };
 }
 setWindowTitle = function(title) {
  document.title = title;
 };
} else {
 throw new Error("environment detection error");
}

var out = Module["print"] || console.log.bind(console);

var err = Module["printErr"] || console.warn.bind(console);

for (key in moduleOverrides) {
 if (moduleOverrides.hasOwnProperty(key)) {
  Module[key] = moduleOverrides[key];
 }
}

moduleOverrides = null;

if (Module["arguments"]) arguments_ = Module["arguments"];

if (!Object.getOwnPropertyDescriptor(Module, "arguments")) Object.defineProperty(Module, "arguments", {
 configurable: true,
 get: function() {
  abort("Module.arguments has been replaced with plain arguments_");
 }
});

if (Module["thisProgram"]) thisProgram = Module["thisProgram"];

if (!Object.getOwnPropertyDescriptor(Module, "thisProgram")) Object.defineProperty(Module, "thisProgram", {
 configurable: true,
 get: function() {
  abort("Module.thisProgram has been replaced with plain thisProgram");
 }
});

if (Module["quit"]) quit_ = Module["quit"];

if (!Object.getOwnPropertyDescriptor(Module, "quit")) Object.defineProperty(Module, "quit", {
 configurable: true,
 get: function() {
  abort("Module.quit has been replaced with plain quit_");
 }
});

assert(typeof Module["memoryInitializerPrefixURL"] === "undefined", "Module.memoryInitializerPrefixURL option was removed, use Module.locateFile instead");

assert(typeof Module["pthreadMainPrefixURL"] === "undefined", "Module.pthreadMainPrefixURL option was removed, use Module.locateFile instead");

assert(typeof Module["cdInitializerPrefixURL"] === "undefined", "Module.cdInitializerPrefixURL option was removed, use Module.locateFile instead");

assert(typeof Module["filePackagePrefixURL"] === "undefined", "Module.filePackagePrefixURL option was removed, use Module.locateFile instead");

assert(typeof Module["read"] === "undefined", "Module.read option was removed (modify read_ in JS)");

assert(typeof Module["readAsync"] === "undefined", "Module.readAsync option was removed (modify readAsync in JS)");

assert(typeof Module["readBinary"] === "undefined", "Module.readBinary option was removed (modify readBinary in JS)");

assert(typeof Module["setWindowTitle"] === "undefined", "Module.setWindowTitle option was removed (modify setWindowTitle in JS)");

if (!Object.getOwnPropertyDescriptor(Module, "read")) Object.defineProperty(Module, "read", {
 configurable: true,
 get: function() {
  abort("Module.read has been replaced with plain read_");
 }
});

if (!Object.getOwnPropertyDescriptor(Module, "readAsync")) Object.defineProperty(Module, "readAsync", {
 configurable: true,
 get: function() {
  abort("Module.readAsync has been replaced with plain readAsync");
 }
});

if (!Object.getOwnPropertyDescriptor(Module, "readBinary")) Object.defineProperty(Module, "readBinary", {
 configurable: true,
 get: function() {
  abort("Module.readBinary has been replaced with plain readBinary");
 }
});

assert(ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER || ENVIRONMENT_IS_NODE, "Pthreads do not work in this environment yet (need Web Workers, or an alternative to them)");

stackSave = stackRestore = stackAlloc = function() {
 abort("cannot use the stack before compiled code is ready to run, and has provided stack access");
};

function warnOnce(text) {
 if (!warnOnce.shown) warnOnce.shown = {};
 if (!warnOnce.shown[text]) {
  warnOnce.shown[text] = 1;
  err(text);
 }
}

var tempRet0 = 0;

var setTempRet0 = function(value) {
 tempRet0 = value;
};

var getTempRet0 = function() {
 return tempRet0;
};

function establishStackSpace(base) {
 stackRestore(base);
}

var Atomics_load = Atomics.load;

var Atomics_store = Atomics.store;

var Atomics_compareExchange = Atomics.compareExchange;

var wasmBinary;

if (Module["wasmBinary"]) wasmBinary = Module["wasmBinary"];

if (!Object.getOwnPropertyDescriptor(Module, "wasmBinary")) Object.defineProperty(Module, "wasmBinary", {
 configurable: true,
 get: function() {
  abort("Module.wasmBinary has been replaced with plain wasmBinary");
 }
});

var noExitRuntime;

if (Module["noExitRuntime"]) noExitRuntime = Module["noExitRuntime"];

if (!Object.getOwnPropertyDescriptor(Module, "noExitRuntime")) Object.defineProperty(Module, "noExitRuntime", {
 configurable: true,
 get: function() {
  abort("Module.noExitRuntime has been replaced with plain noExitRuntime");
 }
});

if (typeof WebAssembly !== "object") {
 abort("No WebAssembly support found. Build with -s WASM=0 to target JavaScript instead.");
}

function setValue(ptr, value, type, noSafe) {
 type = type || "i8";
 if (type.charAt(type.length - 1) === "*") type = "i32";
 switch (type) {
 case "i1":
  HEAP8[ptr >> 0] = value;
  break;

 case "i8":
  HEAP8[ptr >> 0] = value;
  break;

 case "i16":
  HEAP16[ptr >> 1] = value;
  break;

 case "i32":
  HEAP32[ptr >> 2] = value;
  break;

 case "i64":
  tempI64 = [ value >>> 0, (tempDouble = value, +Math_abs(tempDouble) >= 1 ? tempDouble > 0 ? (Math_min(+Math_floor(tempDouble / 4294967296), 4294967295) | 0) >>> 0 : ~~+Math_ceil((tempDouble - +(~~tempDouble >>> 0)) / 4294967296) >>> 0 : 0) ], 
  HEAP32[ptr >> 2] = tempI64[0], HEAP32[ptr + 4 >> 2] = tempI64[1];
  break;

 case "float":
  HEAPF32[ptr >> 2] = value;
  break;

 case "double":
  HEAPF64[ptr >> 3] = value;
  break;

 default:
  abort("invalid type for setValue: " + type);
 }
}

var wasmMemory;

var wasmTable = new WebAssembly.Table({
 "initial": 466,
 "maximum": 466 + 0,
 "element": "anyfunc"
});

var wasmModule;

var threadInfoStruct = 0;

var selfThreadId = 0;

var ABORT = false;

var EXITSTATUS = 0;

function assert(condition, text) {
 if (!condition) {
  abort("Assertion failed: " + text);
 }
}

function getCFunc(ident) {
 var func = Module["_" + ident];
 assert(func, "Cannot call unknown function " + ident + ", make sure it is exported");
 return func;
}

function UTF8ArrayToString(u8Array, idx, maxBytesToRead) {
 var endIdx = idx + maxBytesToRead;
 var str = "";
 while (!(idx >= endIdx)) {
  var u0 = u8Array[idx++];
  if (!u0) return str;
  if (!(u0 & 128)) {
   str += String.fromCharCode(u0);
   continue;
  }
  var u1 = u8Array[idx++] & 63;
  if ((u0 & 224) == 192) {
   str += String.fromCharCode((u0 & 31) << 6 | u1);
   continue;
  }
  var u2 = u8Array[idx++] & 63;
  if ((u0 & 240) == 224) {
   u0 = (u0 & 15) << 12 | u1 << 6 | u2;
  } else {
   if ((u0 & 248) != 240) warnOnce("Invalid UTF-8 leading byte 0x" + u0.toString(16) + " encountered when deserializing a UTF-8 string on the asm.js/wasm heap to a JS string!");
   u0 = (u0 & 7) << 18 | u1 << 12 | u2 << 6 | u8Array[idx++] & 63;
  }
  if (u0 < 65536) {
   str += String.fromCharCode(u0);
  } else {
   var ch = u0 - 65536;
   str += String.fromCharCode(55296 | ch >> 10, 56320 | ch & 1023);
  }
 }
 return str;
}

function UTF8ToString(ptr, maxBytesToRead) {
 return ptr ? UTF8ArrayToString(HEAPU8, ptr, maxBytesToRead) : "";
}

function stringToUTF8Array(str, outU8Array, outIdx, maxBytesToWrite) {
 if (!(maxBytesToWrite > 0)) return 0;
 var startIdx = outIdx;
 var endIdx = outIdx + maxBytesToWrite - 1;
 for (var i = 0; i < str.length; ++i) {
  var u = str.charCodeAt(i);
  if (u >= 55296 && u <= 57343) {
   var u1 = str.charCodeAt(++i);
   u = 65536 + ((u & 1023) << 10) | u1 & 1023;
  }
  if (u <= 127) {
   if (outIdx >= endIdx) break;
   outU8Array[outIdx++] = u;
  } else if (u <= 2047) {
   if (outIdx + 1 >= endIdx) break;
   outU8Array[outIdx++] = 192 | u >> 6;
   outU8Array[outIdx++] = 128 | u & 63;
  } else if (u <= 65535) {
   if (outIdx + 2 >= endIdx) break;
   outU8Array[outIdx++] = 224 | u >> 12;
   outU8Array[outIdx++] = 128 | u >> 6 & 63;
   outU8Array[outIdx++] = 128 | u & 63;
  } else {
   if (outIdx + 3 >= endIdx) break;
   if (u >= 2097152) warnOnce("Invalid Unicode code point 0x" + u.toString(16) + " encountered when serializing a JS string to an UTF-8 string on the asm.js/wasm heap! (Valid unicode code points should be in range 0-0x1FFFFF).");
   outU8Array[outIdx++] = 240 | u >> 18;
   outU8Array[outIdx++] = 128 | u >> 12 & 63;
   outU8Array[outIdx++] = 128 | u >> 6 & 63;
   outU8Array[outIdx++] = 128 | u & 63;
  }
 }
 outU8Array[outIdx] = 0;
 return outIdx - startIdx;
}

function stringToUTF8(str, outPtr, maxBytesToWrite) {
 assert(typeof maxBytesToWrite == "number", "stringToUTF8(str, outPtr, maxBytesToWrite) is missing the third parameter that specifies the length of the output buffer!");
 return stringToUTF8Array(str, HEAPU8, outPtr, maxBytesToWrite);
}

function lengthBytesUTF8(str) {
 var len = 0;
 for (var i = 0; i < str.length; ++i) {
  var u = str.charCodeAt(i);
  if (u >= 55296 && u <= 57343) u = 65536 + ((u & 1023) << 10) | str.charCodeAt(++i) & 1023;
  if (u <= 127) ++len; else if (u <= 2047) len += 2; else if (u <= 65535) len += 3; else len += 4;
 }
 return len;
}

function allocateUTF8(str) {
 var size = lengthBytesUTF8(str) + 1;
 var ret = _malloc(size);
 if (ret) stringToUTF8Array(str, HEAP8, ret, size);
 return ret;
}

function writeArrayToMemory(array, buffer) {
 assert(array.length >= 0, "writeArrayToMemory array must have a length (should be an array or typed array)");
 HEAP8.set(array, buffer);
}

function writeAsciiToMemory(str, buffer, dontAddNull) {
 for (var i = 0; i < str.length; ++i) {
  assert(str.charCodeAt(i) === str.charCodeAt(i) & 255);
  HEAP8[buffer++ >> 0] = str.charCodeAt(i);
 }
 if (!dontAddNull) HEAP8[buffer >> 0] = 0;
}

var WASM_PAGE_SIZE = 65536;

var buffer, HEAP8, HEAPU8, HEAP16, HEAPU16, HEAP32, HEAPU32, HEAPF32, HEAPF64;

function updateGlobalBufferAndViews(buf) {
 buffer = buf;
 Module["HEAP8"] = HEAP8 = new Int8Array(buf);
 Module["HEAP16"] = HEAP16 = new Int16Array(buf);
 Module["HEAP32"] = HEAP32 = new Int32Array(buf);
 Module["HEAPU8"] = HEAPU8 = new Uint8Array(buf);
 Module["HEAPU16"] = HEAPU16 = new Uint16Array(buf);
 Module["HEAPU32"] = HEAPU32 = new Uint32Array(buf);
 Module["HEAPF32"] = HEAPF32 = new Float32Array(buf);
 Module["HEAPF64"] = HEAPF64 = new Float64Array(buf);
}

var STACK_BASE = 5420096, STACKTOP = STACK_BASE, STACK_MAX = 177216, DYNAMIC_BASE = 5420096, DYNAMICTOP_PTR = 176288;

assert(STACK_BASE % 16 === 0, "stack must start aligned");

assert(DYNAMIC_BASE % 16 === 0, "heap must start aligned");

if (ENVIRONMENT_IS_PTHREAD) {
 STACK_MAX = STACKTOP = STACK_MAX = 2147483647;
}

var TOTAL_STACK = 5242880;

if (Module["TOTAL_STACK"]) assert(TOTAL_STACK === Module["TOTAL_STACK"], "the stack size can no longer be determined at runtime");

var INITIAL_TOTAL_MEMORY = Module["TOTAL_MEMORY"] || 16777216;

if (!Object.getOwnPropertyDescriptor(Module, "TOTAL_MEMORY")) Object.defineProperty(Module, "TOTAL_MEMORY", {
 configurable: true,
 get: function() {
  abort("Module.TOTAL_MEMORY has been replaced with plain INITIAL_TOTAL_MEMORY");
 }
});

assert(INITIAL_TOTAL_MEMORY >= TOTAL_STACK, "TOTAL_MEMORY should be larger than TOTAL_STACK, was " + INITIAL_TOTAL_MEMORY + "! (TOTAL_STACK=" + TOTAL_STACK + ")");

assert(typeof Int32Array !== "undefined" && typeof Float64Array !== "undefined" && Int32Array.prototype.subarray !== undefined && Int32Array.prototype.set !== undefined, "JS engine does not provide full typed array support");

if (ENVIRONMENT_IS_PTHREAD) {
 wasmMemory = Module["wasmMemory"];
 buffer = Module["buffer"];
} else {
 if (Module["wasmMemory"]) {
  wasmMemory = Module["wasmMemory"];
 } else {
  wasmMemory = new WebAssembly.Memory({
   "initial": INITIAL_TOTAL_MEMORY / WASM_PAGE_SIZE,
   "maximum": INITIAL_TOTAL_MEMORY / WASM_PAGE_SIZE,
   "shared": true
  });
  if (!(wasmMemory.buffer instanceof SharedArrayBuffer)) {
   err("requested a shared WebAssembly.Memory but the returned buffer is not a SharedArrayBuffer, indicating that while the browser has SharedArrayBuffer it does not have WebAssembly threads support - you may need to set a flag");
   if (ENVIRONMENT_HAS_NODE) {
    console.log("(on node you may need: --experimental-wasm-threads --experimental-wasm-bulk-memory and also use a recent version)");
   }
   throw Error("bad memory");
  }
 }
}

if (wasmMemory) {
 buffer = wasmMemory.buffer;
}

INITIAL_TOTAL_MEMORY = buffer.byteLength;

assert(INITIAL_TOTAL_MEMORY % WASM_PAGE_SIZE === 0);

updateGlobalBufferAndViews(buffer);

if (!ENVIRONMENT_IS_PTHREAD) {
 HEAP32[DYNAMICTOP_PTR >> 2] = DYNAMIC_BASE;
}

function writeStackCookie() {
 assert((STACK_MAX & 3) == 0);
 HEAPU32[(STACK_MAX >> 2) + 1] = 34821223;
 HEAPU32[(STACK_MAX >> 2) + 2] = 2310721022;
 HEAP32[0] = 1668509029;
}

function checkStackCookie() {
 var cookie1 = HEAPU32[(STACK_MAX >> 2) + 1];
 var cookie2 = HEAPU32[(STACK_MAX >> 2) + 2];
 if (cookie1 != 34821223 || cookie2 != 2310721022) {
  abort("Stack overflow! Stack cookie has been overwritten, expected hex dwords 0x89BACDFE and 0x2135467, but received 0x" + cookie2.toString(16) + " " + cookie1.toString(16));
 }
 if (HEAP32[0] !== 1668509029) abort("Runtime error: The application has corrupted its heap memory area (address zero)!");
}

function abortStackOverflow(allocSize) {
 abort("Stack overflow! Attempted to allocate " + allocSize + " bytes on the stack, but stack has only " + (STACK_MAX - stackSave() + allocSize) + " bytes available!");
}

(function() {
 var h16 = new Int16Array(1);
 var h8 = new Int8Array(h16.buffer);
 h16[0] = 25459;
 if (h8[0] !== 115 || h8[1] !== 99) throw "Runtime error: expected the system to be little-endian!";
})();

function callRuntimeCallbacks(callbacks) {
 while (callbacks.length > 0) {
  var callback = callbacks.shift();
  if (typeof callback == "function") {
   callback();
   continue;
  }
  var func = callback.func;
  if (typeof func === "number") {
   if (callback.arg === undefined) {
    Module["dynCall_v"](func);
   } else {
    Module["dynCall_vi"](func, callback.arg);
   }
  } else {
   func(callback.arg === undefined ? null : callback.arg);
  }
 }
}

var __ATPRERUN__ = [];

var __ATINIT__ = [];

var __ATMAIN__ = [];

var __ATEXIT__ = [];

var __ATPOSTRUN__ = [];

var runtimeInitialized = false;

var runtimeExited = false;

if (ENVIRONMENT_IS_PTHREAD) runtimeInitialized = true;

function preRun() {
 if (ENVIRONMENT_IS_PTHREAD) return;
 if (Module["preRun"]) {
  if (typeof Module["preRun"] == "function") Module["preRun"] = [ Module["preRun"] ];
  while (Module["preRun"].length) {
   addOnPreRun(Module["preRun"].shift());
  }
 }
 callRuntimeCallbacks(__ATPRERUN__);
}

function initRuntime() {
 checkStackCookie();
 assert(!runtimeInitialized);
 runtimeInitialized = true;
 callRuntimeCallbacks(__ATINIT__);
}

function preMain() {
 checkStackCookie();
 if (ENVIRONMENT_IS_PTHREAD) return;
 callRuntimeCallbacks(__ATMAIN__);
}

function exitRuntime() {
 checkStackCookie();
 if (ENVIRONMENT_IS_PTHREAD) return;
 runtimeExited = true;
}

function postRun() {
 checkStackCookie();
 if (ENVIRONMENT_IS_PTHREAD) return;
 if (Module["postRun"]) {
  if (typeof Module["postRun"] == "function") Module["postRun"] = [ Module["postRun"] ];
  while (Module["postRun"].length) {
   addOnPostRun(Module["postRun"].shift());
  }
 }
 callRuntimeCallbacks(__ATPOSTRUN__);
}

function addOnPreRun(cb) {
 __ATPRERUN__.unshift(cb);
}

function addOnPostRun(cb) {
 __ATPOSTRUN__.unshift(cb);
}

assert(Math.imul, "This browser does not support Math.imul(), build with LEGACY_VM_SUPPORT or POLYFILL_OLD_MATH_FUNCTIONS to add in a polyfill");

assert(Math.fround, "This browser does not support Math.fround(), build with LEGACY_VM_SUPPORT or POLYFILL_OLD_MATH_FUNCTIONS to add in a polyfill");

assert(Math.clz32, "This browser does not support Math.clz32(), build with LEGACY_VM_SUPPORT or POLYFILL_OLD_MATH_FUNCTIONS to add in a polyfill");

assert(Math.trunc, "This browser does not support Math.trunc(), build with LEGACY_VM_SUPPORT or POLYFILL_OLD_MATH_FUNCTIONS to add in a polyfill");

var Math_abs = Math.abs;

var Math_ceil = Math.ceil;

var Math_floor = Math.floor;

var Math_min = Math.min;

var runDependencies = 0;

var runDependencyWatcher = null;

var dependenciesFulfilled = null;

var runDependencyTracking = {};

function addRunDependency(id) {
 assert(!ENVIRONMENT_IS_PTHREAD, "addRunDependency cannot be used in a pthread worker");
 runDependencies++;
 if (Module["monitorRunDependencies"]) {
  Module["monitorRunDependencies"](runDependencies);
 }
 if (id) {
  assert(!runDependencyTracking[id]);
  runDependencyTracking[id] = 1;
  if (runDependencyWatcher === null && typeof setInterval !== "undefined") {
   runDependencyWatcher = setInterval(function() {
    if (ABORT) {
     clearInterval(runDependencyWatcher);
     runDependencyWatcher = null;
     return;
    }
    var shown = false;
    for (var dep in runDependencyTracking) {
     if (!shown) {
      shown = true;
      err("still waiting on run dependencies:");
     }
     err("dependency: " + dep);
    }
    if (shown) {
     err("(end of list)");
    }
   }, 1e4);
  }
 } else {
  err("warning: run dependency added without ID");
 }
}

function removeRunDependency(id) {
 runDependencies--;
 if (Module["monitorRunDependencies"]) {
  Module["monitorRunDependencies"](runDependencies);
 }
 if (id) {
  assert(runDependencyTracking[id]);
  delete runDependencyTracking[id];
 } else {
  err("warning: run dependency removed without ID");
 }
 if (runDependencies == 0) {
  if (runDependencyWatcher !== null) {
   clearInterval(runDependencyWatcher);
   runDependencyWatcher = null;
  }
  if (dependenciesFulfilled) {
   var callback = dependenciesFulfilled;
   dependenciesFulfilled = null;
   callback();
  }
 }
}

Module["preloadedImages"] = {};

Module["preloadedAudios"] = {};

function abort(what) {
 if (Module["onAbort"]) {
  Module["onAbort"](what);
 }
 if (ENVIRONMENT_IS_PTHREAD) console.error("Pthread aborting at " + new Error().stack);
 what += "";
 out(what);
 err(what);
 ABORT = true;
 EXITSTATUS = 1;
 var output = "abort(" + what + ") at " + stackTrace();
 what = output;
 throw new WebAssembly.RuntimeError(what);
}

if (!ENVIRONMENT_IS_PTHREAD) addOnPreRun(function() {
 if (typeof SharedArrayBuffer !== "undefined") {
  addRunDependency("pthreads");
  PThread.allocateUnusedWorkers(1, function() {
   removeRunDependency("pthreads");
  });
 }
});

var FS = {
 error: function() {
  abort("Filesystem support (FS) was not included. The problem is that you are using files from JS, but files were not used from C/C++, so filesystem support was not auto-included. You can force-include filesystem support with  -s FORCE_FILESYSTEM=1");
 },
 init: function() {
  FS.error();
 },
 createDataFile: function() {
  FS.error();
 },
 createPreloadedFile: function() {
  FS.error();
 },
 createLazyFile: function() {
  FS.error();
 },
 open: function() {
  FS.error();
 },
 mkdev: function() {
  FS.error();
 },
 registerDevice: function() {
  FS.error();
 },
 analyzePath: function() {
  FS.error();
 },
 loadFilesFromDB: function() {
  FS.error();
 },
 ErrnoError: function ErrnoError() {
  FS.error();
 }
};

Module["FS_createDataFile"] = FS.createDataFile;

Module["FS_createPreloadedFile"] = FS.createPreloadedFile;

var dataURIPrefix = "data:application/octet-stream;base64,";

function isDataURI(filename) {
 return String.prototype.startsWith ? filename.startsWith(dataURIPrefix) : filename.indexOf(dataURIPrefix) === 0;
}

var wasmBinaryFile = "libr3.wasm";

if (!isDataURI(wasmBinaryFile)) {
 wasmBinaryFile = locateFile(wasmBinaryFile);
}

function getBinary() {
 try {
  if (wasmBinary) {
   return new Uint8Array(wasmBinary);
  }
  if (readBinary) {
   return readBinary(wasmBinaryFile);
  } else {
   throw "both async and sync fetching of the wasm failed";
  }
 } catch (err) {
  abort(err);
 }
}

function getBinaryPromise() {
 if (!wasmBinary && (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) && typeof fetch === "function") {
  return fetch(wasmBinaryFile, {
   credentials: "same-origin"
  }).then(function(response) {
   if (!response["ok"]) {
    throw "failed to load wasm binary file at '" + wasmBinaryFile + "'";
   }
   return response["arrayBuffer"]();
  }).catch(function() {
   return getBinary();
  });
 }
 return new Promise(function(resolve, reject) {
  resolve(getBinary());
 });
}

function createWasm() {
 var info = {
  "env": asmLibraryArg,
  "wasi_snapshot_preview1": asmLibraryArg
 };
 function receiveInstance(instance, module) {
  var exports = instance.exports;
  Module["asm"] = exports;
  wasmModule = module;
  if (!ENVIRONMENT_IS_PTHREAD) removeRunDependency("wasm-instantiate");
 }
 if (!ENVIRONMENT_IS_PTHREAD) {
  addRunDependency("wasm-instantiate");
 }
 var trueModule = Module;
 function receiveInstantiatedSource(output) {
  assert(Module === trueModule, "the Module object should not be replaced during async compilation - perhaps the order of HTML elements is wrong?");
  trueModule = null;
  receiveInstance(output["instance"], output["module"]);
 }
 function instantiateArrayBuffer(receiver) {
  return getBinaryPromise().then(function(binary) {
   return WebAssembly.instantiate(binary, info);
  }).then(receiver, function(reason) {
   err("failed to asynchronously prepare wasm: " + reason);
   abort(reason);
  });
 }
 function instantiateAsync() {
  if (!wasmBinary && typeof WebAssembly.instantiateStreaming === "function" && !isDataURI(wasmBinaryFile) && typeof fetch === "function") {
   fetch(wasmBinaryFile, {
    credentials: "same-origin"
   }).then(function(response) {
    var result = WebAssembly.instantiateStreaming(response, info);
    return result.then(receiveInstantiatedSource, function(reason) {
     err("wasm streaming compile failed: " + reason);
     err("falling back to ArrayBuffer instantiation");
     instantiateArrayBuffer(receiveInstantiatedSource);
    });
   });
  } else {
   return instantiateArrayBuffer(receiveInstantiatedSource);
  }
 }
 if (Module["instantiateWasm"]) {
  try {
   var exports = Module["instantiateWasm"](info, receiveInstance);
   return exports;
  } catch (e) {
   err("Module.instantiateWasm callback failed with error: " + e);
   return false;
  }
 }
 instantiateAsync();
 return {};
}

var tempDouble;

var tempI64;

var ASM_CONSTS = {
 146820: function($0, $1) {
  reb.RejectPromise_internal($0, $1);
 },
 147001: function($0, $1) {
  reb.ResolvePromise_internal($0, $1);
 },
 147193: function($0) {
  return reb.GetNativeResult_internal($0);
 },
 147680: function($0, $1) {
  reb.RunNative_internal($0, $1);
 },
 147715: function($0, $1) {
  reb.RunNative_internal($0, $1);
  _RL_rebTakeAwaitLock_internal();
 },
 148091: function($0) {
  return reb.GetNativeError_internal($0);
 },
 148423: function($0) {
  try {
   eval(UTF8ToString($0));
   return null;
  } catch (e) {
   return reb.Value("make error!", reb.T(e.toString()));
  }
 },
 148599: function($0) {
  eval(UTF8ToString($0));
 },
 148626: function($0) {
  (1, eval)(UTF8ToString($0));
 },
 148657: function($0) {
  return reb.Box(eval(UTF8ToString($0)));
 },
 148700: function($0) {
  return reb.Box((1, eval)(UTF8ToString($0)));
 },
 149258: function($0) {
  reb.UnregisterId_internal($0);
 },
 156112: function() {
  throw "Canceled!";
 }
};

function _emscripten_asm_const_async_on_main_thread_vii(code, sigPtr, argbuf) {
 if (ENVIRONMENT_IS_PTHREAD) {
  return _emscripten_proxy_to_main_thread_js(-1 - code, 0, code, sigPtr, argbuf);
 }
 var args = _emscripten_read_asm_const_args(sigPtr, argbuf);
 return ASM_CONSTS[code].apply(null, args);
}

function _emscripten_asm_const_sync_on_main_thread_iii(code, sigPtr, argbuf) {
 if (ENVIRONMENT_IS_PTHREAD) {
  return _emscripten_proxy_to_main_thread_js(-1 - code, 1, code, sigPtr, argbuf);
 }
 var args = _emscripten_read_asm_const_args(sigPtr, argbuf);
 return ASM_CONSTS[code].apply(null, args);
}

function _emscripten_asm_const_iii(code, sigPtr, argbuf) {
 var args = _emscripten_read_asm_const_args(sigPtr, argbuf);
 return ASM_CONSTS[code].apply(null, args);
}

function initPthreadsJS() {
 PThread.initRuntime();
}

if (!ENVIRONMENT_IS_PTHREAD) __ATINIT__.push({
 func: function() {
  ___wasm_call_ctors();
 }
});

function demangle(func) {
 warnOnce("warning: build with  -s DEMANGLE_SUPPORT=1  to link in libcxxabi demangling");
 return func;
}

function demangleAll(text) {
 var regex = /\b_Z[\w\d_]+/g;
 return text.replace(regex, function(x) {
  var y = demangle(x);
  return x === y ? x : y + " [" + x + "]";
 });
}

var PROCINFO = {
 ppid: 1,
 pid: 42,
 sid: 42,
 pgid: 42
};

var __pthread_ptr = 0;

var __pthread_is_main_runtime_thread = 0;

var __pthread_is_main_browser_thread = 0;

function __register_pthread_ptr(pthreadPtr, isMainBrowserThread, isMainRuntimeThread) {
 pthreadPtr = pthreadPtr | 0;
 isMainBrowserThread = isMainBrowserThread | 0;
 isMainRuntimeThread = isMainRuntimeThread | 0;
 __pthread_ptr = pthreadPtr;
 __pthread_is_main_browser_thread = isMainBrowserThread;
 __pthread_is_main_runtime_thread = isMainRuntimeThread;
}

Module["__register_pthread_ptr"] = __register_pthread_ptr;

var ERRNO_CODES = {
 EPERM: 63,
 ENOENT: 44,
 ESRCH: 71,
 EINTR: 27,
 EIO: 29,
 ENXIO: 60,
 E2BIG: 1,
 ENOEXEC: 45,
 EBADF: 8,
 ECHILD: 12,
 EAGAIN: 6,
 EWOULDBLOCK: 6,
 ENOMEM: 48,
 EACCES: 2,
 EFAULT: 21,
 ENOTBLK: 105,
 EBUSY: 10,
 EEXIST: 20,
 EXDEV: 75,
 ENODEV: 43,
 ENOTDIR: 54,
 EISDIR: 31,
 EINVAL: 28,
 ENFILE: 41,
 EMFILE: 33,
 ENOTTY: 59,
 ETXTBSY: 74,
 EFBIG: 22,
 ENOSPC: 51,
 ESPIPE: 70,
 EROFS: 69,
 EMLINK: 34,
 EPIPE: 64,
 EDOM: 18,
 ERANGE: 68,
 ENOMSG: 49,
 EIDRM: 24,
 ECHRNG: 106,
 EL2NSYNC: 156,
 EL3HLT: 107,
 EL3RST: 108,
 ELNRNG: 109,
 EUNATCH: 110,
 ENOCSI: 111,
 EL2HLT: 112,
 EDEADLK: 16,
 ENOLCK: 46,
 EBADE: 113,
 EBADR: 114,
 EXFULL: 115,
 ENOANO: 104,
 EBADRQC: 103,
 EBADSLT: 102,
 EDEADLOCK: 16,
 EBFONT: 101,
 ENOSTR: 100,
 ENODATA: 116,
 ETIME: 117,
 ENOSR: 118,
 ENONET: 119,
 ENOPKG: 120,
 EREMOTE: 121,
 ENOLINK: 47,
 EADV: 122,
 ESRMNT: 123,
 ECOMM: 124,
 EPROTO: 65,
 EMULTIHOP: 36,
 EDOTDOT: 125,
 EBADMSG: 9,
 ENOTUNIQ: 126,
 EBADFD: 127,
 EREMCHG: 128,
 ELIBACC: 129,
 ELIBBAD: 130,
 ELIBSCN: 131,
 ELIBMAX: 132,
 ELIBEXEC: 133,
 ENOSYS: 52,
 ENOTEMPTY: 55,
 ENAMETOOLONG: 37,
 ELOOP: 32,
 EOPNOTSUPP: 138,
 EPFNOSUPPORT: 139,
 ECONNRESET: 15,
 ENOBUFS: 42,
 EAFNOSUPPORT: 5,
 EPROTOTYPE: 67,
 ENOTSOCK: 57,
 ENOPROTOOPT: 50,
 ESHUTDOWN: 140,
 ECONNREFUSED: 14,
 EADDRINUSE: 3,
 ECONNABORTED: 13,
 ENETUNREACH: 40,
 ENETDOWN: 38,
 ETIMEDOUT: 73,
 EHOSTDOWN: 142,
 EHOSTUNREACH: 23,
 EINPROGRESS: 26,
 EALREADY: 7,
 EDESTADDRREQ: 17,
 EMSGSIZE: 35,
 EPROTONOSUPPORT: 66,
 ESOCKTNOSUPPORT: 137,
 EADDRNOTAVAIL: 4,
 ENETRESET: 39,
 EISCONN: 30,
 ENOTCONN: 53,
 ETOOMANYREFS: 141,
 EUSERS: 136,
 EDQUOT: 19,
 ESTALE: 72,
 ENOTSUP: 138,
 ENOMEDIUM: 148,
 EILSEQ: 25,
 EOVERFLOW: 61,
 ECANCELED: 11,
 ENOTRECOVERABLE: 56,
 EOWNERDEAD: 62,
 ESTRPIPE: 135
};

var __main_thread_futex_wait_address = 177200;

function _emscripten_futex_wake(addr, count) {
 if (addr <= 0 || addr > HEAP8.length || addr & 3 != 0 || count < 0) return -28;
 if (count == 0) return 0;
 if (count >= 2147483647) count = Infinity;
 var mainThreadWaitAddress = Atomics.load(HEAP32, __main_thread_futex_wait_address >> 2);
 var mainThreadWoken = 0;
 if (mainThreadWaitAddress == addr) {
  var loadedAddr = Atomics.compareExchange(HEAP32, __main_thread_futex_wait_address >> 2, mainThreadWaitAddress, 0);
  if (loadedAddr == mainThreadWaitAddress) {
   --count;
   mainThreadWoken = 1;
   if (count <= 0) return 1;
  }
 }
 var ret = Atomics.notify(HEAP32, addr >> 2, count);
 if (ret >= 0) return ret + mainThreadWoken;
 throw "Atomics.notify returned an unexpected value " + ret;
}

Module["_emscripten_futex_wake"] = _emscripten_futex_wake;

function __kill_thread(pthread_ptr) {
 if (ENVIRONMENT_IS_PTHREAD) throw "Internal Error! _kill_thread() can only ever be called from main application thread!";
 if (!pthread_ptr) throw "Internal Error! Null pthread_ptr in _kill_thread!";
 HEAP32[pthread_ptr + 12 >> 2] = 0;
 var pthread = PThread.pthreads[pthread_ptr];
 pthread.worker.terminate();
 PThread.freeThreadData(pthread);
 PThread.runningWorkers.splice(PThread.runningWorkers.indexOf(pthread.worker), 1);
 pthread.worker.pthread = undefined;
}

function __cancel_thread(pthread_ptr) {
 if (ENVIRONMENT_IS_PTHREAD) throw "Internal Error! _cancel_thread() can only ever be called from main application thread!";
 if (!pthread_ptr) throw "Internal Error! Null pthread_ptr in _cancel_thread!";
 var pthread = PThread.pthreads[pthread_ptr];
 pthread.worker.postMessage({
  "cmd": "cancel"
 });
}

function __cleanup_thread(pthread_ptr) {
 if (ENVIRONMENT_IS_PTHREAD) throw "Internal Error! _cleanup_thread() can only ever be called from main application thread!";
 if (!pthread_ptr) throw "Internal Error! Null pthread_ptr in _cleanup_thread!";
 HEAP32[pthread_ptr + 12 >> 2] = 0;
 var pthread = PThread.pthreads[pthread_ptr];
 if (pthread) {
  var worker = pthread.worker;
  PThread.returnWorkerToPool(worker);
 }
}

var PThread = {
 MAIN_THREAD_ID: 1,
 mainThreadInfo: {
  schedPolicy: 0,
  schedPrio: 0
 },
 preallocatedWorkers: [],
 unusedWorkers: [],
 runningWorkers: [],
 initRuntime: function() {
  __register_pthread_ptr(PThread.mainThreadBlock, !ENVIRONMENT_IS_WORKER, 1);
  _emscripten_register_main_browser_thread_id(PThread.mainThreadBlock);
 },
 initMainThreadBlock: function() {
  if (ENVIRONMENT_IS_PTHREAD) return undefined;
  var requestedPoolSize = 1;
  PThread.preallocatedWorkers = PThread.createNewWorkers(requestedPoolSize);
  PThread.mainThreadBlock = 176448;
  for (var i = 0; i < 232 / 4; ++i) HEAPU32[PThread.mainThreadBlock / 4 + i] = 0;
  HEAP32[PThread.mainThreadBlock + 12 >> 2] = PThread.mainThreadBlock;
  var headPtr = PThread.mainThreadBlock + 156;
  HEAP32[headPtr >> 2] = headPtr;
  var tlsMemory = 176688;
  for (var i = 0; i < 128; ++i) HEAPU32[tlsMemory / 4 + i] = 0;
  Atomics.store(HEAPU32, PThread.mainThreadBlock + 104 >> 2, tlsMemory);
  Atomics.store(HEAPU32, PThread.mainThreadBlock + 40 >> 2, PThread.mainThreadBlock);
  Atomics.store(HEAPU32, PThread.mainThreadBlock + 44 >> 2, PROCINFO.pid);
 },
 initWorker: function() {},
 pthreads: {},
 exitHandlers: null,
 setThreadStatus: function() {},
 runExitHandlers: function() {
  if (PThread.exitHandlers !== null) {
   while (PThread.exitHandlers.length > 0) {
    PThread.exitHandlers.pop()();
   }
   PThread.exitHandlers = null;
  }
  if (ENVIRONMENT_IS_PTHREAD && threadInfoStruct) ___pthread_tsd_run_dtors();
 },
 threadExit: function(exitCode) {
  var tb = _pthread_self();
  if (tb) {
   err("Pthread 0x" + tb.toString(16) + " exited.");
   Atomics.store(HEAPU32, tb + 4 >> 2, exitCode);
   Atomics.store(HEAPU32, tb + 0 >> 2, 1);
   Atomics.store(HEAPU32, tb + 60 >> 2, 1);
   Atomics.store(HEAPU32, tb + 64 >> 2, 0);
   PThread.runExitHandlers();
   _emscripten_futex_wake(tb + 0, 2147483647);
   __register_pthread_ptr(0, 0, 0);
   threadInfoStruct = 0;
   if (ENVIRONMENT_IS_PTHREAD) {
    postMessage({
     "cmd": "exit"
    });
   }
  }
 },
 threadCancel: function() {
  PThread.runExitHandlers();
  Atomics.store(HEAPU32, threadInfoStruct + 4 >> 2, -1);
  Atomics.store(HEAPU32, threadInfoStruct + 0 >> 2, 1);
  _emscripten_futex_wake(threadInfoStruct + 0, 2147483647);
  threadInfoStruct = selfThreadId = 0;
  __register_pthread_ptr(0, 0, 0);
  postMessage({
   "cmd": "cancelDone"
  });
 },
 terminateAllThreads: function() {
  for (var t in PThread.pthreads) {
   var pthread = PThread.pthreads[t];
   if (pthread && pthread.worker) {
    PThread.returnWorkerToPool(pthread.worker);
   }
  }
  PThread.pthreads = {};
  for (var i = 0; i < PThread.preallocatedWorkers.length; ++i) {
   var worker = PThread.preallocatedWorkers[i];
   assert(!worker.pthread);
   worker.terminate();
  }
  PThread.preallocatedWorkers = [];
  for (var i = 0; i < PThread.unusedWorkers.length; ++i) {
   var worker = PThread.unusedWorkers[i];
   assert(!worker.pthread);
   worker.terminate();
  }
  PThread.unusedWorkers = [];
  for (var i = 0; i < PThread.runningWorkers.length; ++i) {
   var worker = PThread.runningWorkers[i];
   var pthread = worker.pthread;
   assert(pthread, "This Worker should have a pthread it is executing");
   PThread.freeThreadData(pthread);
   worker.terminate();
  }
  PThread.runningWorkers = [];
 },
 freeThreadData: function(pthread) {
  if (!pthread) return;
  if (pthread.threadInfoStruct) {
   var tlsMemory = HEAP32[pthread.threadInfoStruct + 104 >> 2];
   HEAP32[pthread.threadInfoStruct + 104 >> 2] = 0;
   _free(tlsMemory);
   _free(pthread.threadInfoStruct);
  }
  pthread.threadInfoStruct = 0;
  if (pthread.allocatedOwnStack && pthread.stackBase) _free(pthread.stackBase);
  pthread.stackBase = 0;
  if (pthread.worker) pthread.worker.pthread = null;
 },
 returnWorkerToPool: function(worker) {
  delete PThread.pthreads[worker.pthread.thread];
  PThread.unusedWorkers.push(worker);
  PThread.runningWorkers.splice(PThread.runningWorkers.indexOf(worker), 1);
  PThread.freeThreadData(worker.pthread);
  worker.pthread = undefined;
 },
 receiveObjectTransfer: function(data) {},
 allocateUnusedWorkers: function(numWorkers, onFinishedLoading) {
  if (typeof SharedArrayBuffer === "undefined") return;
  var workers = [];
  var numWorkersToCreate = numWorkers;
  if (PThread.preallocatedWorkers.length > 0) {
   var workersUsed = Math.min(PThread.preallocatedWorkers.length, numWorkers);
   workers = workers.concat(PThread.preallocatedWorkers.splice(0, workersUsed));
   numWorkersToCreate -= workersUsed;
  }
  if (numWorkersToCreate > 0) {
   workers = workers.concat(PThread.createNewWorkers(numWorkersToCreate));
  }
  PThread.attachListenerToWorkers(workers, onFinishedLoading);
  for (var i = 0; i < numWorkers; ++i) {
   var worker = workers[i];
   worker.postMessage({
    "cmd": "load",
    "urlOrBlob": Module["mainScriptUrlOrBlob"] || _scriptDir,
    "wasmMemory": wasmMemory,
    "wasmModule": wasmModule,
    "DYNAMIC_BASE": DYNAMIC_BASE,
    "DYNAMICTOP_PTR": DYNAMICTOP_PTR
   });
   PThread.unusedWorkers.push(worker);
  }
 },
 attachListenerToWorkers: function(workers, onFinishedLoading) {
  var numWorkersLoaded = 0;
  var numWorkers = workers.length;
  for (var i = 0; i < numWorkers; ++i) {
   var worker = workers[i];
   (function(worker) {
    worker.onmessage = function(e) {
     var d = e["data"];
     var cmd = d["cmd"];
     if (worker.pthread) PThread.currentProxiedOperationCallerThread = worker.pthread.threadInfoStruct;
     if (d["targetThread"] && d["targetThread"] != _pthread_self()) {
      var thread = PThread.pthreads[d.targetThread];
      if (thread) {
       thread.worker.postMessage(e.data, d["transferList"]);
      } else {
       console.error('Internal error! Worker sent a message "' + cmd + '" to target pthread ' + d["targetThread"] + ", but that thread no longer exists!");
      }
      PThread.currentProxiedOperationCallerThread = undefined;
      return;
     }
     if (cmd === "processQueuedMainThreadWork") {
      _emscripten_main_thread_process_queued_calls();
     } else if (cmd === "spawnThread") {
      __spawn_thread(e.data);
     } else if (cmd === "cleanupThread") {
      __cleanup_thread(d["thread"]);
     } else if (cmd === "killThread") {
      __kill_thread(d["thread"]);
     } else if (cmd === "cancelThread") {
      __cancel_thread(d["thread"]);
     } else if (cmd === "loaded") {
      worker.loaded = true;
      if (worker.runPthread) {
       worker.runPthread();
       delete worker.runPthread;
      }
      ++numWorkersLoaded;
      if (numWorkersLoaded === numWorkers && onFinishedLoading) {
       onFinishedLoading();
      }
     } else if (cmd === "print") {
      out("Thread " + d["threadId"] + ": " + d["text"]);
     } else if (cmd === "printErr") {
      err("Thread " + d["threadId"] + ": " + d["text"]);
     } else if (cmd === "alert") {
      alert("Thread " + d["threadId"] + ": " + d["text"]);
     } else if (cmd === "exit") {
      var detached = worker.pthread && Atomics.load(HEAPU32, worker.pthread.thread + 68 >> 2);
      if (detached) {
       PThread.returnWorkerToPool(worker);
      }
     } else if (cmd === "cancelDone") {
      PThread.returnWorkerToPool(worker);
     } else if (cmd === "objectTransfer") {
      PThread.receiveObjectTransfer(e.data);
     } else if (e.data.target === "setimmediate") {
      worker.postMessage(e.data);
     } else {
      err("worker sent an unknown command " + cmd);
     }
     PThread.currentProxiedOperationCallerThread = undefined;
    };
    worker.onerror = function(e) {
     err("pthread sent an error! " + e.filename + ":" + e.lineno + ": " + e.message);
    };
   })(worker);
  }
 },
 createNewWorkers: function(numWorkers) {
  if (typeof SharedArrayBuffer === "undefined") return [];
  var pthreadMainJs = "libr3.worker.js";
  pthreadMainJs = locateFile(pthreadMainJs);
  var newWorkers = [];
  for (var i = 0; i < numWorkers; ++i) {
   newWorkers.push(new Worker(pthreadMainJs));
  }
  return newWorkers;
 },
 getNewWorker: function() {
  if (PThread.unusedWorkers.length == 0) PThread.allocateUnusedWorkers(1);
  if (PThread.unusedWorkers.length > 0) return PThread.unusedWorkers.pop(); else return null;
 },
 busySpinWait: function(msecs) {
  var t = performance.now() + msecs;
  while (performance.now() < t) {}
 }
};

function establishStackSpaceInJsModule(stackTop, stackMax) {
 STACK_BASE = STACKTOP = stackTop;
 STACK_MAX = stackMax;
 ___set_stack_limit(STACK_MAX);
 writeStackCookie();
 establishStackSpace(stackTop, stackMax);
}

Module["establishStackSpaceInJsModule"] = establishStackSpaceInJsModule;

function jsStackTrace() {
 var err = new Error();
 if (!err.stack) {
  try {
   throw new Error(0);
  } catch (e) {
   err = e;
  }
  if (!err.stack) {
   return "(no stack trace available)";
  }
 }
 return err.stack.toString();
}

function stackTrace() {
 var js = jsStackTrace();
 if (Module["extraStackTrace"]) js += "\n" + Module["extraStackTrace"]();
 return demangleAll(js);
}

function ___assert_fail(condition, filename, line, func) {
 abort("Assertion failed: " + UTF8ToString(condition) + ", at: " + [ filename ? UTF8ToString(filename) : "unknown filename", line, func ? UTF8ToString(func) : "unknown function" ]);
}

function _emscripten_get_now() {
 abort();
}

function ___handle_stack_overflow() {
 abort("stack overflow");
}

function ___lock() {}

function ___unlock() {}

function __emscripten_notify_thread_queue(targetThreadId, mainThreadId) {
 if (targetThreadId == mainThreadId) {
  postMessage({
   cmd: "processQueuedMainThreadWork"
  });
 } else if (ENVIRONMENT_IS_PTHREAD) {
  postMessage({
   targetThread: targetThreadId,
   cmd: "processThreadQueue"
  });
 } else {
  var pthread = PThread.pthreads[targetThreadId];
  var worker = pthread && pthread.worker;
  if (!worker) {
   err("Cannot send message to thread with ID " + targetThreadId + ", unknown thread ID!");
   return;
  }
  worker.postMessage({
   cmd: "processThreadQueue"
  });
 }
 return 1;
}

function _emscripten_check_blocking_allowed() {
 assert(ENVIRONMENT_IS_WEB);
 warnOnce("Blocking on the main thread is very dangerous, see https://emscripten.org/docs/porting/pthreads.html#blocking-on-the-main-browser-thread");
}

function _emscripten_conditional_set_current_thread_status(expectedStatus, newStatus) {
 expectedStatus = expectedStatus | 0;
 newStatus = newStatus | 0;
}

function _emscripten_futex_wait(addr, val, timeout) {
 if (addr <= 0 || addr > HEAP8.length || addr & 3 != 0) return -28;
 if (ENVIRONMENT_IS_WORKER) {
  var ret = Atomics.wait(HEAP32, addr >> 2, val, timeout);
  if (ret === "timed-out") return -73;
  if (ret === "not-equal") return -6;
  if (ret === "ok") return 0;
  throw "Atomics.wait returned an unexpected value " + ret;
 } else {
  var loadedVal = Atomics.load(HEAP32, addr >> 2);
  if (val != loadedVal) return -6;
  var tNow = performance.now();
  var tEnd = tNow + timeout;
  Atomics.store(HEAP32, __main_thread_futex_wait_address >> 2, addr);
  var ourWaitAddress = addr;
  while (addr == ourWaitAddress) {
   tNow = performance.now();
   if (tNow > tEnd) {
    return -73;
   }
   _emscripten_main_thread_process_queued_calls();
   addr = Atomics.load(HEAP32, __main_thread_futex_wait_address >> 2);
  }
  return 0;
 }
}

function _emscripten_is_main_browser_thread() {
 return __pthread_is_main_browser_thread | 0;
}

function _emscripten_is_main_runtime_thread() {
 return __pthread_is_main_runtime_thread | 0;
}

var setjmpId = 0;

function _saveSetjmp(env, label, table, size) {
 env = env | 0;
 label = label | 0;
 table = table | 0;
 size = size | 0;
 var i = 0;
 setjmpId = setjmpId + 1 | 0;
 HEAP32[env >> 2] = setjmpId;
 while ((i | 0) < (size | 0)) {
  if ((HEAP32[table + (i << 3) >> 2] | 0) == 0) {
   HEAP32[table + (i << 3) >> 2] = setjmpId;
   HEAP32[table + ((i << 3) + 4) >> 2] = label;
   HEAP32[table + ((i << 3) + 8) >> 2] = 0;
   setTempRet0(size | 0);
   return table | 0;
  }
  i = i + 1 | 0;
 }
 size = size * 2 | 0;
 table = _realloc(table | 0, 8 * (size + 1 | 0) | 0) | 0;
 table = _saveSetjmp(env | 0, label | 0, table | 0, size | 0) | 0;
 setTempRet0(size | 0);
 return table | 0;
}

function _testSetjmp(id, table, size) {
 id = id | 0;
 table = table | 0;
 size = size | 0;
 var i = 0, curr = 0;
 while ((i | 0) < (size | 0)) {
  curr = HEAP32[table + (i << 3) >> 2] | 0;
  if ((curr | 0) == 0) break;
  if ((curr | 0) == (id | 0)) {
   return HEAP32[table + ((i << 3) + 4) >> 2] | 0;
  }
  i = i + 1 | 0;
 }
 return 0;
}

function _longjmp(env, value) {
 _setThrew(env, value || 1);
 throw "longjmp";
}

function _emscripten_longjmp(env, value) {
 _longjmp(env, value);
}

function _emscripten_memcpy_big(dest, src, num) {
 HEAPU8.set(HEAPU8.subarray(src, src + num), dest);
}

function _emscripten_proxy_to_main_thread_js(index, sync) {
 var numCallArgs = arguments.length - 2;
 if (numCallArgs > 20 - 1) throw "emscripten_proxy_to_main_thread_js: Too many arguments " + numCallArgs + " to proxied function idx=" + index + ", maximum supported is " + (20 - 1) + "!";
 var stack = stackSave();
 var args = stackAlloc(numCallArgs * 8);
 var b = args >> 3;
 for (var i = 0; i < numCallArgs; i++) {
  HEAPF64[b + i] = arguments[2 + i];
 }
 var ret = _emscripten_run_in_main_runtime_thread_js(index, numCallArgs, args, sync);
 stackRestore(stack);
 return ret;
}

var _emscripten_receive_on_main_thread_js_callArgs = [];

function _emscripten_read_asm_const_args(sigPtr, buf) {
 if (!_emscripten_read_asm_const_args.array) {
  _emscripten_read_asm_const_args.array = [];
 }
 var args = _emscripten_read_asm_const_args.array;
 args.length = 0;
 var ch;
 while (ch = HEAPU8[sigPtr++]) {
  if (ch === 100 || ch === 102) {
   buf = buf + 7 & ~7;
   args.push(HEAPF64[buf >> 3]);
   buf += 8;
  } else if (ch === 105) {
   buf = buf + 3 & ~3;
   args.push(HEAP32[buf >> 2]);
   buf += 4;
  } else abort("unexpected char in asm const signature " + ch);
 }
 return args;
}

function _emscripten_receive_on_main_thread_js(index, numCallArgs, args) {
 _emscripten_receive_on_main_thread_js_callArgs.length = numCallArgs;
 var b = args >> 3;
 for (var i = 0; i < numCallArgs; i++) {
  _emscripten_receive_on_main_thread_js_callArgs[i] = HEAPF64[b + i];
 }
 var isEmAsmConst = index < 0;
 var func = !isEmAsmConst ? proxiedFunctionTable[index] : ASM_CONSTS[-index - 1];
 if (isEmAsmConst) {
  var sigPtr = _emscripten_receive_on_main_thread_js_callArgs[1];
  var varargPtr = _emscripten_receive_on_main_thread_js_callArgs[2];
  var constArgs = _emscripten_read_asm_const_args(sigPtr, varargPtr);
  return func.apply(null, constArgs);
 }
 assert(func.length == numCallArgs, "Call args mismatch in emscripten_receive_on_main_thread_js");
 return func.apply(null, _emscripten_receive_on_main_thread_js_callArgs);
}

function abortOnCannotGrowMemory(requestedSize) {
 abort("Cannot enlarge memory arrays to size " + requestedSize + " bytes (OOM). Either (1) compile with  -s TOTAL_MEMORY=X  with X higher than the current value " + HEAP8.length + ", (2) compile with  -s ALLOW_MEMORY_GROWTH=1  which allows increasing the size at runtime, or (3) if you want malloc to return NULL (0) instead of this abort, compile with  -s ABORTING_MALLOC=0 ");
}

function _emscripten_resize_heap(requestedSize) {
 abortOnCannotGrowMemory(requestedSize);
}

var JSEvents = {
 keyEvent: 0,
 mouseEvent: 0,
 wheelEvent: 0,
 uiEvent: 0,
 focusEvent: 0,
 deviceOrientationEvent: 0,
 deviceMotionEvent: 0,
 fullscreenChangeEvent: 0,
 pointerlockChangeEvent: 0,
 visibilityChangeEvent: 0,
 touchEvent: 0,
 previousFullscreenElement: null,
 previousScreenX: null,
 previousScreenY: null,
 removeEventListenersRegistered: false,
 removeAllEventListeners: function() {
  for (var i = JSEvents.eventHandlers.length - 1; i >= 0; --i) {
   JSEvents._removeHandler(i);
  }
  JSEvents.eventHandlers = [];
  JSEvents.deferredCalls = [];
 },
 registerRemoveEventListeners: function() {
  if (!JSEvents.removeEventListenersRegistered) {
   __ATEXIT__.push(JSEvents.removeAllEventListeners);
   JSEvents.removeEventListenersRegistered = true;
  }
 },
 deferredCalls: [],
 deferCall: function(targetFunction, precedence, argsList) {
  function arraysHaveEqualContent(arrA, arrB) {
   if (arrA.length != arrB.length) return false;
   for (var i in arrA) {
    if (arrA[i] != arrB[i]) return false;
   }
   return true;
  }
  for (var i in JSEvents.deferredCalls) {
   var call = JSEvents.deferredCalls[i];
   if (call.targetFunction == targetFunction && arraysHaveEqualContent(call.argsList, argsList)) {
    return;
   }
  }
  JSEvents.deferredCalls.push({
   targetFunction: targetFunction,
   precedence: precedence,
   argsList: argsList
  });
  JSEvents.deferredCalls.sort(function(x, y) {
   return x.precedence < y.precedence;
  });
 },
 removeDeferredCalls: function(targetFunction) {
  for (var i = 0; i < JSEvents.deferredCalls.length; ++i) {
   if (JSEvents.deferredCalls[i].targetFunction == targetFunction) {
    JSEvents.deferredCalls.splice(i, 1);
    --i;
   }
  }
 },
 canPerformEventHandlerRequests: function() {
  return JSEvents.inEventHandler && JSEvents.currentEventHandler.allowsDeferredCalls;
 },
 runDeferredCalls: function() {
  if (!JSEvents.canPerformEventHandlerRequests()) {
   return;
  }
  for (var i = 0; i < JSEvents.deferredCalls.length; ++i) {
   var call = JSEvents.deferredCalls[i];
   JSEvents.deferredCalls.splice(i, 1);
   --i;
   call.targetFunction.apply(this, call.argsList);
  }
 },
 inEventHandler: 0,
 currentEventHandler: null,
 eventHandlers: [],
 removeAllHandlersOnTarget: function(target, eventTypeString) {
  for (var i = 0; i < JSEvents.eventHandlers.length; ++i) {
   if (JSEvents.eventHandlers[i].target == target && (!eventTypeString || eventTypeString == JSEvents.eventHandlers[i].eventTypeString)) {
    JSEvents._removeHandler(i--);
   }
  }
 },
 _removeHandler: function(i) {
  var h = JSEvents.eventHandlers[i];
  h.target.removeEventListener(h.eventTypeString, h.eventListenerFunc, h.useCapture);
  JSEvents.eventHandlers.splice(i, 1);
 },
 registerOrRemoveHandler: function(eventHandler) {
  var jsEventHandler = function jsEventHandler(event) {
   ++JSEvents.inEventHandler;
   JSEvents.currentEventHandler = eventHandler;
   JSEvents.runDeferredCalls();
   eventHandler.handlerFunc(event);
   JSEvents.runDeferredCalls();
   --JSEvents.inEventHandler;
  };
  if (eventHandler.callbackfunc) {
   eventHandler.eventListenerFunc = jsEventHandler;
   eventHandler.target.addEventListener(eventHandler.eventTypeString, jsEventHandler, eventHandler.useCapture);
   JSEvents.eventHandlers.push(eventHandler);
   JSEvents.registerRemoveEventListeners();
  } else {
   for (var i = 0; i < JSEvents.eventHandlers.length; ++i) {
    if (JSEvents.eventHandlers[i].target == eventHandler.target && JSEvents.eventHandlers[i].eventTypeString == eventHandler.eventTypeString) {
     JSEvents._removeHandler(i--);
    }
   }
  }
 },
 queueEventHandlerOnThread_iiii: function(targetThread, eventHandlerFunc, eventTypeId, eventData, userData) {
  var stackTop = stackSave();
  var varargs = stackAlloc(12);
  HEAP32[varargs >> 2] = eventTypeId;
  HEAP32[varargs + 4 >> 2] = eventData;
  HEAP32[varargs + 8 >> 2] = userData;
  _emscripten_async_queue_on_thread_(targetThread, 637534208, eventHandlerFunc, eventData, varargs);
  stackRestore(stackTop);
 },
 getTargetThreadForEventCallback: function(targetThread) {
  switch (targetThread) {
  case 1:
   return 0;

  case 2:
   return PThread.currentProxiedOperationCallerThread;

  default:
   return targetThread;
  }
 },
 getNodeNameForTarget: function(target) {
  if (!target) return "";
  if (target == window) return "#window";
  if (target == screen) return "#screen";
  return target && target.nodeName ? target.nodeName : "";
 },
 fullscreenEnabled: function() {
  return document.fullscreenEnabled || document.webkitFullscreenEnabled;
 }
};

function stringToNewUTF8(jsString) {
 var length = lengthBytesUTF8(jsString) + 1;
 var cString = _malloc(length);
 stringToUTF8(jsString, cString, length);
 return cString;
}

function _emscripten_set_offscreencanvas_size_on_target_thread_js(targetThread, targetCanvas, width, height) {
 var stackTop = stackSave();
 var varargs = stackAlloc(12);
 var targetCanvasPtr = 0;
 if (targetCanvas) {
  targetCanvasPtr = stringToNewUTF8(targetCanvas);
 }
 HEAP32[varargs >> 2] = targetCanvasPtr;
 HEAP32[varargs + 4 >> 2] = width;
 HEAP32[varargs + 8 >> 2] = height;
 _emscripten_async_queue_on_thread_(targetThread, 657457152, 0, targetCanvasPtr, varargs);
 stackRestore(stackTop);
}

function _emscripten_set_offscreencanvas_size_on_target_thread(targetThread, targetCanvas, width, height) {
 targetCanvas = targetCanvas ? UTF8ToString(targetCanvas) : "";
 _emscripten_set_offscreencanvas_size_on_target_thread_js(targetThread, targetCanvas, width, height);
}

function __maybeCStringToJsString(cString) {
 return cString === cString + 0 ? UTF8ToString(cString) : cString;
}

var __specialEventTargets = [ 0, typeof document !== "undefined" ? document : 0, typeof window !== "undefined" ? window : 0 ];

function __findEventTarget(target) {
 var domElement = __specialEventTargets[target] || (typeof document !== "undefined" ? document.querySelector(__maybeCStringToJsString(target)) : undefined);
 return domElement;
}

function __findCanvasEventTarget(target) {
 return __findEventTarget(target);
}

function _emscripten_set_canvas_element_size_calling_thread(target, width, height) {
 var canvas = __findCanvasEventTarget(target);
 if (!canvas) return -4;
 if (canvas.canvasSharedPtr) {
  HEAP32[canvas.canvasSharedPtr >> 2] = width;
  HEAP32[canvas.canvasSharedPtr + 4 >> 2] = height;
 }
 if (canvas.offscreenCanvas || !canvas.controlTransferredOffscreen) {
  if (canvas.offscreenCanvas) canvas = canvas.offscreenCanvas;
  var autoResizeViewport = false;
  if (canvas.GLctxObject && canvas.GLctxObject.GLctx) {
   var prevViewport = canvas.GLctxObject.GLctx.getParameter(2978);
   autoResizeViewport = prevViewport[0] === 0 && prevViewport[1] === 0 && prevViewport[2] === canvas.width && prevViewport[3] === canvas.height;
  }
  canvas.width = width;
  canvas.height = height;
  if (autoResizeViewport) {
   canvas.GLctxObject.GLctx.viewport(0, 0, width, height);
  }
 } else if (canvas.canvasSharedPtr) {
  var targetThread = HEAP32[canvas.canvasSharedPtr + 8 >> 2];
  _emscripten_set_offscreencanvas_size_on_target_thread(targetThread, target, width, height);
  return 1;
 } else {
  return -4;
 }
 return 0;
}

function _emscripten_set_canvas_element_size_main_thread(target, width, height) {
 if (ENVIRONMENT_IS_PTHREAD) return _emscripten_proxy_to_main_thread_js(1, 1, target, width, height);
 return _emscripten_set_canvas_element_size_calling_thread(target, width, height);
}

function _emscripten_set_canvas_element_size(target, width, height) {
 var canvas = __findCanvasEventTarget(target);
 if (canvas) {
  return _emscripten_set_canvas_element_size_calling_thread(target, width, height);
 } else {
  return _emscripten_set_canvas_element_size_main_thread(target, width, height);
 }
}

function _emscripten_set_current_thread_status(newStatus) {
 newStatus = newStatus | 0;
}

function _emscripten_syscall(which, varargs) {
 switch (which) {
 default:
  throw "surprising proxied syscall: " + which;
 }
}

function __webgl_acquireInstancedArraysExtension(ctx) {
 var ext = ctx.getExtension("ANGLE_instanced_arrays");
 if (ext) {
  ctx["vertexAttribDivisor"] = function(index, divisor) {
   ext["vertexAttribDivisorANGLE"](index, divisor);
  };
  ctx["drawArraysInstanced"] = function(mode, first, count, primcount) {
   ext["drawArraysInstancedANGLE"](mode, first, count, primcount);
  };
  ctx["drawElementsInstanced"] = function(mode, count, type, indices, primcount) {
   ext["drawElementsInstancedANGLE"](mode, count, type, indices, primcount);
  };
 }
}

function __webgl_acquireVertexArrayObjectExtension(ctx) {
 var ext = ctx.getExtension("OES_vertex_array_object");
 if (ext) {
  ctx["createVertexArray"] = function() {
   return ext["createVertexArrayOES"]();
  };
  ctx["deleteVertexArray"] = function(vao) {
   ext["deleteVertexArrayOES"](vao);
  };
  ctx["bindVertexArray"] = function(vao) {
   ext["bindVertexArrayOES"](vao);
  };
  ctx["isVertexArray"] = function(vao) {
   return ext["isVertexArrayOES"](vao);
  };
 }
}

function __webgl_acquireDrawBuffersExtension(ctx) {
 var ext = ctx.getExtension("WEBGL_draw_buffers");
 if (ext) {
  ctx["drawBuffers"] = function(n, bufs) {
   ext["drawBuffersWEBGL"](n, bufs);
  };
 }
}

var GL = {
 counter: 1,
 lastError: 0,
 buffers: [],
 mappedBuffers: {},
 programs: [],
 framebuffers: [],
 renderbuffers: [],
 textures: [],
 uniforms: [],
 shaders: [],
 vaos: [],
 contexts: {},
 currentContext: null,
 offscreenCanvases: {},
 timerQueriesEXT: [],
 programInfos: {},
 stringCache: {},
 unpackAlignment: 4,
 init: function() {
  var miniTempFloatBuffer = new Float32Array(GL.MINI_TEMP_BUFFER_SIZE);
  for (var i = 0; i < GL.MINI_TEMP_BUFFER_SIZE; i++) {
   GL.miniTempBufferFloatViews[i] = miniTempFloatBuffer.subarray(0, i + 1);
  }
  var miniTempIntBuffer = new Int32Array(GL.MINI_TEMP_BUFFER_SIZE);
  for (var i = 0; i < GL.MINI_TEMP_BUFFER_SIZE; i++) {
   GL.miniTempBufferIntViews[i] = miniTempIntBuffer.subarray(0, i + 1);
  }
 },
 recordError: function recordError(errorCode) {
  if (!GL.lastError) {
   GL.lastError = errorCode;
  }
 },
 getNewId: function(table) {
  var ret = GL.counter++;
  for (var i = table.length; i < ret; i++) {
   table[i] = null;
  }
  return ret;
 },
 MINI_TEMP_BUFFER_SIZE: 256,
 miniTempBufferFloatViews: [ 0 ],
 miniTempBufferIntViews: [ 0 ],
 getSource: function(shader, count, string, length) {
  var source = "";
  for (var i = 0; i < count; ++i) {
   var len = length ? HEAP32[length + i * 4 >> 2] : -1;
   source += UTF8ToString(HEAP32[string + i * 4 >> 2], len < 0 ? undefined : len);
  }
  return source;
 },
 createContext: function(canvas, webGLContextAttributes) {
  var ctx = canvas.getContext("webgl", webGLContextAttributes);
  if (!ctx) return 0;
  var handle = GL.registerContext(ctx, webGLContextAttributes);
  return handle;
 },
 registerContext: function(ctx, webGLContextAttributes) {
  var handle = _malloc(8);
  HEAP32[handle + 4 >> 2] = _pthread_self();
  var context = {
   handle: handle,
   attributes: webGLContextAttributes,
   version: webGLContextAttributes.majorVersion,
   GLctx: ctx
  };
  if (ctx.canvas) ctx.canvas.GLctxObject = context;
  GL.contexts[handle] = context;
  if (typeof webGLContextAttributes.enableExtensionsByDefault === "undefined" || webGLContextAttributes.enableExtensionsByDefault) {
   GL.initExtensions(context);
  }
  return handle;
 },
 makeContextCurrent: function(contextHandle) {
  GL.currentContext = GL.contexts[contextHandle];
  Module.ctx = GLctx = GL.currentContext && GL.currentContext.GLctx;
  return !(contextHandle && !GLctx);
 },
 getContext: function(contextHandle) {
  return GL.contexts[contextHandle];
 },
 deleteContext: function(contextHandle) {
  if (GL.currentContext === GL.contexts[contextHandle]) GL.currentContext = null;
  if (typeof JSEvents === "object") JSEvents.removeAllHandlersOnTarget(GL.contexts[contextHandle].GLctx.canvas);
  if (GL.contexts[contextHandle] && GL.contexts[contextHandle].GLctx.canvas) GL.contexts[contextHandle].GLctx.canvas.GLctxObject = undefined;
  _free(GL.contexts[contextHandle]);
  GL.contexts[contextHandle] = null;
 },
 initExtensions: function(context) {
  if (!context) context = GL.currentContext;
  if (context.initExtensionsDone) return;
  context.initExtensionsDone = true;
  var GLctx = context.GLctx;
  if (context.version < 2) {
   __webgl_acquireInstancedArraysExtension(GLctx);
   __webgl_acquireVertexArrayObjectExtension(GLctx);
   __webgl_acquireDrawBuffersExtension(GLctx);
  }
  GLctx.disjointTimerQueryExt = GLctx.getExtension("EXT_disjoint_timer_query");
  var automaticallyEnabledExtensions = [ "OES_texture_float", "OES_texture_half_float", "OES_standard_derivatives", "OES_vertex_array_object", "WEBGL_compressed_texture_s3tc", "WEBGL_depth_texture", "OES_element_index_uint", "EXT_texture_filter_anisotropic", "EXT_frag_depth", "WEBGL_draw_buffers", "ANGLE_instanced_arrays", "OES_texture_float_linear", "OES_texture_half_float_linear", "EXT_blend_minmax", "EXT_shader_texture_lod", "WEBGL_compressed_texture_pvrtc", "EXT_color_buffer_half_float", "WEBGL_color_buffer_float", "EXT_sRGB", "WEBGL_compressed_texture_etc1", "EXT_disjoint_timer_query", "WEBGL_compressed_texture_etc", "WEBGL_compressed_texture_astc", "EXT_color_buffer_float", "WEBGL_compressed_texture_s3tc_srgb", "EXT_disjoint_timer_query_webgl2", "WEBKIT_WEBGL_compressed_texture_pvrtc" ];
  var exts = GLctx.getSupportedExtensions() || [];
  exts.forEach(function(ext) {
   if (automaticallyEnabledExtensions.indexOf(ext) != -1) {
    GLctx.getExtension(ext);
   }
  });
 },
 populateUniformTable: function(program) {
  var p = GL.programs[program];
  var ptable = GL.programInfos[program] = {
   uniforms: {},
   maxUniformLength: 0,
   maxAttributeLength: -1,
   maxUniformBlockNameLength: -1
  };
  var utable = ptable.uniforms;
  var numUniforms = GLctx.getProgramParameter(p, 35718);
  for (var i = 0; i < numUniforms; ++i) {
   var u = GLctx.getActiveUniform(p, i);
   var name = u.name;
   ptable.maxUniformLength = Math.max(ptable.maxUniformLength, name.length + 1);
   if (name.slice(-1) == "]") {
    name = name.slice(0, name.lastIndexOf("["));
   }
   var loc = GLctx.getUniformLocation(p, name);
   if (loc) {
    var id = GL.getNewId(GL.uniforms);
    utable[name] = [ u.size, id ];
    GL.uniforms[id] = loc;
    for (var j = 1; j < u.size; ++j) {
     var n = name + "[" + j + "]";
     loc = GLctx.getUniformLocation(p, n);
     id = GL.getNewId(GL.uniforms);
     GL.uniforms[id] = loc;
    }
   }
  }
 }
};

var __emscripten_webgl_power_preferences = [ "default", "low-power", "high-performance" ];

function _emscripten_webgl_do_create_context(target, attributes) {
 assert(attributes);
 var contextAttributes = {};
 var a = attributes >> 2;
 contextAttributes["alpha"] = !!HEAP32[a + (0 >> 2)];
 contextAttributes["depth"] = !!HEAP32[a + (4 >> 2)];
 contextAttributes["stencil"] = !!HEAP32[a + (8 >> 2)];
 contextAttributes["antialias"] = !!HEAP32[a + (12 >> 2)];
 contextAttributes["premultipliedAlpha"] = !!HEAP32[a + (16 >> 2)];
 contextAttributes["preserveDrawingBuffer"] = !!HEAP32[a + (20 >> 2)];
 var powerPreference = HEAP32[a + (24 >> 2)];
 contextAttributes["powerPreference"] = __emscripten_webgl_power_preferences[powerPreference];
 contextAttributes["failIfMajorPerformanceCaveat"] = !!HEAP32[a + (28 >> 2)];
 contextAttributes.majorVersion = HEAP32[a + (32 >> 2)];
 contextAttributes.minorVersion = HEAP32[a + (36 >> 2)];
 contextAttributes.enableExtensionsByDefault = HEAP32[a + (40 >> 2)];
 contextAttributes.explicitSwapControl = HEAP32[a + (44 >> 2)];
 contextAttributes.proxyContextToMainThread = HEAP32[a + (48 >> 2)];
 contextAttributes.renderViaOffscreenBackBuffer = HEAP32[a + (52 >> 2)];
 var canvas = __findCanvasEventTarget(target);
 if (!canvas) {
  return 0;
 }
 if (contextAttributes.explicitSwapControl) {
  return 0;
 }
 var contextHandle = GL.createContext(canvas, contextAttributes);
 return contextHandle;
}

function _emscripten_webgl_create_context(a0, a1) {
 return _emscripten_webgl_do_create_context(a0, a1);
}

var ENV = {};

function _emscripten_get_environ() {
 if (!_emscripten_get_environ.strings) {
  var env = {
   "USER": "web_user",
   "LOGNAME": "web_user",
   "PATH": "/",
   "PWD": "/",
   "HOME": "/home/web_user",
   "LANG": (typeof navigator === "object" && navigator.languages && navigator.languages[0] || "C").replace("-", "_") + ".UTF-8",
   "_": thisProgram
  };
  for (var x in ENV) {
   env[x] = ENV[x];
  }
  var strings = [];
  for (var x in env) {
   strings.push(x + "=" + env[x]);
  }
  _emscripten_get_environ.strings = strings;
 }
 return _emscripten_get_environ.strings;
}

function _environ_get(__environ, environ_buf) {
 var strings = _emscripten_get_environ();
 var bufSize = 0;
 strings.forEach(function(string, i) {
  var ptr = environ_buf + bufSize;
  HEAP32[__environ + i * 4 >> 2] = ptr;
  writeAsciiToMemory(string, ptr);
  bufSize += string.length + 1;
 });
 return 0;
}

function _environ_sizes_get(penviron_count, penviron_buf_size) {
 var strings = _emscripten_get_environ();
 HEAP32[penviron_count >> 2] = strings.length;
 var bufSize = 0;
 strings.forEach(function(string) {
  bufSize += string.length + 1;
 });
 HEAP32[penviron_buf_size >> 2] = bufSize;
 return 0;
}

function _exit(status) {
 exit(status);
}

var PATH = {
 splitPath: function(filename) {
  var splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
  return splitPathRe.exec(filename).slice(1);
 },
 normalizeArray: function(parts, allowAboveRoot) {
  var up = 0;
  for (var i = parts.length - 1; i >= 0; i--) {
   var last = parts[i];
   if (last === ".") {
    parts.splice(i, 1);
   } else if (last === "..") {
    parts.splice(i, 1);
    up++;
   } else if (up) {
    parts.splice(i, 1);
    up--;
   }
  }
  if (allowAboveRoot) {
   for (;up; up--) {
    parts.unshift("..");
   }
  }
  return parts;
 },
 normalize: function(path) {
  var isAbsolute = path.charAt(0) === "/", trailingSlash = path.substr(-1) === "/";
  path = PATH.normalizeArray(path.split("/").filter(function(p) {
   return !!p;
  }), !isAbsolute).join("/");
  if (!path && !isAbsolute) {
   path = ".";
  }
  if (path && trailingSlash) {
   path += "/";
  }
  return (isAbsolute ? "/" : "") + path;
 },
 dirname: function(path) {
  var result = PATH.splitPath(path), root = result[0], dir = result[1];
  if (!root && !dir) {
   return ".";
  }
  if (dir) {
   dir = dir.substr(0, dir.length - 1);
  }
  return root + dir;
 },
 basename: function(path) {
  if (path === "/") return "/";
  var lastSlash = path.lastIndexOf("/");
  if (lastSlash === -1) return path;
  return path.substr(lastSlash + 1);
 },
 extname: function(path) {
  return PATH.splitPath(path)[3];
 },
 join: function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return PATH.normalize(paths.join("/"));
 },
 join2: function(l, r) {
  return PATH.normalize(l + "/" + r);
 }
};

var SYSCALLS = {
 buffers: [ null, [], [] ],
 printChar: function(stream, curr) {
  var buffer = SYSCALLS.buffers[stream];
  assert(buffer);
  if (curr === 0 || curr === 10) {
   (stream === 1 ? out : err)(UTF8ArrayToString(buffer, 0));
   buffer.length = 0;
  } else {
   buffer.push(curr);
  }
 },
 varargs: 0,
 get: function(varargs) {
  SYSCALLS.varargs += 4;
  var ret = HEAP32[SYSCALLS.varargs - 4 >> 2];
  return ret;
 },
 getStr: function() {
  var ret = UTF8ToString(SYSCALLS.get());
  return ret;
 },
 get64: function() {
  var low = SYSCALLS.get(), high = SYSCALLS.get();
  if (low >= 0) assert(high === 0); else assert(high === -1);
  return low;
 },
 getZero: function() {
  assert(SYSCALLS.get() === 0);
 }
};

function _fd_close(fd) {
 if (ENVIRONMENT_IS_PTHREAD) return _emscripten_proxy_to_main_thread_js(2, 1, fd);
 try {
  abort("it should not be possible to operate on streams when !SYSCALLS_REQUIRE_FILESYSTEM");
  return 0;
 } catch (e) {
  if (typeof FS === "undefined" || !(e instanceof FS.ErrnoError)) abort(e);
  return e.errno;
 }
}

function _fd_seek(fd, offset_low, offset_high, whence, newOffset) {
 if (ENVIRONMENT_IS_PTHREAD) return _emscripten_proxy_to_main_thread_js(3, 1, fd, offset_low, offset_high, whence, newOffset);
 try {
  abort("it should not be possible to operate on streams when !SYSCALLS_REQUIRE_FILESYSTEM");
  return 0;
 } catch (e) {
  if (typeof FS === "undefined" || !(e instanceof FS.ErrnoError)) abort(e);
  return e.errno;
 }
}

function flush_NO_FILESYSTEM() {
 var fflush = Module["_fflush"];
 if (fflush) fflush(0);
 var buffers = SYSCALLS.buffers;
 if (buffers[1].length) SYSCALLS.printChar(1, 10);
 if (buffers[2].length) SYSCALLS.printChar(2, 10);
}

function _fd_write(fd, iov, iovcnt, pnum) {
 if (ENVIRONMENT_IS_PTHREAD) return _emscripten_proxy_to_main_thread_js(4, 1, fd, iov, iovcnt, pnum);
 try {
  var num = 0;
  for (var i = 0; i < iovcnt; i++) {
   var ptr = HEAP32[iov + i * 8 >> 2];
   var len = HEAP32[iov + (i * 8 + 4) >> 2];
   for (var j = 0; j < len; j++) {
    SYSCALLS.printChar(fd, HEAPU8[ptr + j]);
   }
   num += len;
  }
  HEAP32[pnum >> 2] = num;
  return 0;
 } catch (e) {
  if (typeof FS === "undefined" || !(e instanceof FS.ErrnoError)) abort(e);
  return e.errno;
 }
}

function _getTempRet0() {
 return getTempRet0() | 0;
}

function __spawn_thread(threadParams) {
 if (ENVIRONMENT_IS_PTHREAD) throw "Internal Error! _spawn_thread() can only ever be called from main application thread!";
 var worker = PThread.getNewWorker();
 if (worker.pthread !== undefined) throw "Internal error!";
 if (!threadParams.pthread_ptr) throw "Internal error, no pthread ptr!";
 PThread.runningWorkers.push(worker);
 var tlsMemory = _malloc(128 * 4);
 for (var i = 0; i < 128; ++i) {
  HEAP32[tlsMemory + i * 4 >> 2] = 0;
 }
 var stackHigh = threadParams.stackBase + threadParams.stackSize;
 var pthread = PThread.pthreads[threadParams.pthread_ptr] = {
  worker: worker,
  stackBase: threadParams.stackBase,
  stackSize: threadParams.stackSize,
  allocatedOwnStack: threadParams.allocatedOwnStack,
  thread: threadParams.pthread_ptr,
  threadInfoStruct: threadParams.pthread_ptr
 };
 var tis = pthread.threadInfoStruct >> 2;
 Atomics.store(HEAPU32, tis + (0 >> 2), 0);
 Atomics.store(HEAPU32, tis + (4 >> 2), 0);
 Atomics.store(HEAPU32, tis + (8 >> 2), 0);
 Atomics.store(HEAPU32, tis + (68 >> 2), threadParams.detached);
 Atomics.store(HEAPU32, tis + (104 >> 2), tlsMemory);
 Atomics.store(HEAPU32, tis + (48 >> 2), 0);
 Atomics.store(HEAPU32, tis + (40 >> 2), pthread.threadInfoStruct);
 Atomics.store(HEAPU32, tis + (44 >> 2), PROCINFO.pid);
 Atomics.store(HEAPU32, tis + (108 >> 2), threadParams.stackSize);
 Atomics.store(HEAPU32, tis + (84 >> 2), threadParams.stackSize);
 Atomics.store(HEAPU32, tis + (80 >> 2), stackHigh);
 Atomics.store(HEAPU32, tis + (108 + 8 >> 2), stackHigh);
 Atomics.store(HEAPU32, tis + (108 + 12 >> 2), threadParams.detached);
 Atomics.store(HEAPU32, tis + (108 + 20 >> 2), threadParams.schedPolicy);
 Atomics.store(HEAPU32, tis + (108 + 24 >> 2), threadParams.schedPrio);
 var global_libc = _emscripten_get_global_libc();
 var global_locale = global_libc + 40;
 Atomics.store(HEAPU32, tis + (176 >> 2), global_locale);
 worker.pthread = pthread;
 var msg = {
  "cmd": "run",
  "start_routine": threadParams.startRoutine,
  "arg": threadParams.arg,
  "threadInfoStruct": threadParams.pthread_ptr,
  "selfThreadId": threadParams.pthread_ptr,
  "parentThreadId": threadParams.parent_pthread_ptr,
  "stackBase": threadParams.stackBase,
  "stackSize": threadParams.stackSize
 };
 worker.runPthread = function() {
  msg.time = performance.now();
  worker.postMessage(msg, threadParams.transferList);
 };
 if (worker.loaded) {
  worker.runPthread();
  delete worker.runPthread;
 }
}

function _pthread_getschedparam(thread, policy, schedparam) {
 if (!policy && !schedparam) return ERRNO_CODES.EINVAL;
 if (!thread) {
  err("pthread_getschedparam called with a null thread pointer!");
  return ERRNO_CODES.ESRCH;
 }
 var self = HEAP32[thread + 12 >> 2];
 if (self !== thread) {
  err("pthread_getschedparam attempted on thread " + thread + ", which does not point to a valid thread, or does not exist anymore!");
  return ERRNO_CODES.ESRCH;
 }
 var schedPolicy = Atomics.load(HEAPU32, thread + 108 + 20 >> 2);
 var schedPrio = Atomics.load(HEAPU32, thread + 108 + 24 >> 2);
 if (policy) HEAP32[policy >> 2] = schedPolicy;
 if (schedparam) HEAP32[schedparam >> 2] = schedPrio;
 return 0;
}

function _pthread_self() {
 return __pthread_ptr | 0;
}

Module["_pthread_self"] = _pthread_self;

function _pthread_create(pthread_ptr, attr, start_routine, arg) {
 if (typeof SharedArrayBuffer === "undefined") {
  err("Current environment does not support SharedArrayBuffer, pthreads are not available!");
  return 6;
 }
 if (!pthread_ptr) {
  err("pthread_create called with a null thread pointer!");
  return 28;
 }
 var transferList = [];
 var error = 0;
 if (ENVIRONMENT_IS_PTHREAD && (transferList.length === 0 || error)) {
  return _emscripten_sync_run_in_main_thread_4(687865856, pthread_ptr, attr, start_routine, arg);
 }
 if (error) return error;
 var stackSize = 0;
 var stackBase = 0;
 var detached = 0;
 var schedPolicy = 0;
 var schedPrio = 0;
 if (attr) {
  stackSize = HEAP32[attr >> 2];
  stackSize += 81920;
  stackBase = HEAP32[attr + 8 >> 2];
  detached = HEAP32[attr + 12 >> 2] !== 0;
  var inheritSched = HEAP32[attr + 16 >> 2] === 0;
  if (inheritSched) {
   var prevSchedPolicy = HEAP32[attr + 20 >> 2];
   var prevSchedPrio = HEAP32[attr + 24 >> 2];
   var parentThreadPtr = PThread.currentProxiedOperationCallerThread ? PThread.currentProxiedOperationCallerThread : _pthread_self();
   _pthread_getschedparam(parentThreadPtr, attr + 20, attr + 24);
   schedPolicy = HEAP32[attr + 20 >> 2];
   schedPrio = HEAP32[attr + 24 >> 2];
   HEAP32[attr + 20 >> 2] = prevSchedPolicy;
   HEAP32[attr + 24 >> 2] = prevSchedPrio;
  } else {
   schedPolicy = HEAP32[attr + 20 >> 2];
   schedPrio = HEAP32[attr + 24 >> 2];
  }
 } else {
  stackSize = 2097152;
 }
 var allocatedOwnStack = stackBase == 0;
 if (allocatedOwnStack) {
  stackBase = _memalign(16, stackSize);
 } else {
  stackBase -= stackSize;
  assert(stackBase > 0);
 }
 var threadInfoStruct = _malloc(232);
 for (var i = 0; i < 232 >> 2; ++i) HEAPU32[(threadInfoStruct >> 2) + i] = 0;
 HEAP32[pthread_ptr >> 2] = threadInfoStruct;
 HEAP32[threadInfoStruct + 12 >> 2] = threadInfoStruct;
 var headPtr = threadInfoStruct + 156;
 HEAP32[headPtr >> 2] = headPtr;
 var threadParams = {
  stackBase: stackBase,
  stackSize: stackSize,
  allocatedOwnStack: allocatedOwnStack,
  schedPolicy: schedPolicy,
  schedPrio: schedPrio,
  detached: detached,
  startRoutine: start_routine,
  pthread_ptr: threadInfoStruct,
  parent_pthread_ptr: _pthread_self(),
  arg: arg,
  transferList: transferList
 };
 if (ENVIRONMENT_IS_PTHREAD) {
  threadParams.cmd = "spawnThread";
  postMessage(threadParams, transferList);
 } else {
  __spawn_thread(threadParams);
 }
 return 0;
}

function _setTempRet0($i) {
 setTempRet0($i | 0);
}

if (!ENVIRONMENT_IS_PTHREAD) PThread.initMainThreadBlock(); else PThread.initWorker();

if (ENVIRONMENT_IS_PTHREAD) {
 _emscripten_get_now = function() {
  return performance["now"]() - Module["__performance_now_clock_drift"];
 };
} else _emscripten_get_now = function() {
 return performance["now"]();
};

var GLctx;

GL.init();

var proxiedFunctionTable = [ null, _emscripten_set_canvas_element_size_main_thread, _fd_close, _fd_seek, _fd_write ];

var asmLibraryArg = {
 "__assert_fail": ___assert_fail,
 "__handle_stack_overflow": ___handle_stack_overflow,
 "__lock": ___lock,
 "__unlock": ___unlock,
 "_emscripten_notify_thread_queue": __emscripten_notify_thread_queue,
 "emscripten_asm_const_async_on_main_thread_vii": _emscripten_asm_const_async_on_main_thread_vii,
 "emscripten_asm_const_iii": _emscripten_asm_const_iii,
 "emscripten_asm_const_sync_on_main_thread_iii": _emscripten_asm_const_sync_on_main_thread_iii,
 "emscripten_check_blocking_allowed": _emscripten_check_blocking_allowed,
 "emscripten_conditional_set_current_thread_status": _emscripten_conditional_set_current_thread_status,
 "emscripten_futex_wait": _emscripten_futex_wait,
 "emscripten_futex_wake": _emscripten_futex_wake,
 "emscripten_get_now": _emscripten_get_now,
 "emscripten_is_main_browser_thread": _emscripten_is_main_browser_thread,
 "emscripten_is_main_runtime_thread": _emscripten_is_main_runtime_thread,
 "emscripten_longjmp": _emscripten_longjmp,
 "emscripten_memcpy_big": _emscripten_memcpy_big,
 "emscripten_receive_on_main_thread_js": _emscripten_receive_on_main_thread_js,
 "emscripten_resize_heap": _emscripten_resize_heap,
 "emscripten_set_canvas_element_size": _emscripten_set_canvas_element_size,
 "emscripten_set_current_thread_status": _emscripten_set_current_thread_status,
 "emscripten_syscall": _emscripten_syscall,
 "emscripten_webgl_create_context": _emscripten_webgl_create_context,
 "environ_get": _environ_get,
 "environ_sizes_get": _environ_sizes_get,
 "exit": _exit,
 "fd_close": _fd_close,
 "fd_seek": _fd_seek,
 "fd_write": _fd_write,
 "getTempRet0": _getTempRet0,
 "initPthreadsJS": initPthreadsJS,
 "invoke_i": invoke_i,
 "invoke_ii": invoke_ii,
 "invoke_iii": invoke_iii,
 "invoke_vi": invoke_vi,
 "invoke_vii": invoke_vii,
 "memory": wasmMemory,
 "pthread_create": _pthread_create,
 "pthread_self": _pthread_self,
 "saveSetjmp": _saveSetjmp,
 "setTempRet0": _setTempRet0,
 "table": wasmTable,
 "testSetjmp": _testSetjmp
};

var asm = createWasm();

var real____wasm_call_ctors = asm["__wasm_call_ctors"];

asm["__wasm_call_ctors"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real____wasm_call_ctors.apply(null, arguments);
};

var real__RL_rebEnterApi_internal = asm["RL_rebEnterApi_internal"];

asm["RL_rebEnterApi_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebEnterApi_internal.apply(null, arguments);
};

var real__RL_rebMalloc = asm["RL_rebMalloc"];

asm["RL_rebMalloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebMalloc.apply(null, arguments);
};

var real__RL_rebRealloc = asm["RL_rebRealloc"];

asm["RL_rebRealloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebRealloc.apply(null, arguments);
};

var real__RL_rebFree = asm["RL_rebFree"];

asm["RL_rebFree"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebFree.apply(null, arguments);
};

var real__RL_rebJumps = asm["RL_rebJumps"];

asm["RL_rebJumps"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebJumps.apply(null, arguments);
};

var real__RL_rebRepossess = asm["RL_rebRepossess"];

asm["RL_rebRepossess"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebRepossess.apply(null, arguments);
};

var real__RL_rebStartup = asm["RL_rebStartup"];

asm["RL_rebStartup"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebStartup.apply(null, arguments);
};

var real__RL_rebShutdown = asm["RL_rebShutdown"];

asm["RL_rebShutdown"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebShutdown.apply(null, arguments);
};

var real__RL_rebTick = asm["RL_rebTick"];

asm["RL_rebTick"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebTick.apply(null, arguments);
};

var real__RL_rebVoid = asm["RL_rebVoid"];

asm["RL_rebVoid"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebVoid.apply(null, arguments);
};

var real__RL_rebBlank = asm["RL_rebBlank"];

asm["RL_rebBlank"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebBlank.apply(null, arguments);
};

var real__RL_rebLogic = asm["RL_rebLogic"];

asm["RL_rebLogic"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebLogic.apply(null, arguments);
};

var real__RL_rebChar = asm["RL_rebChar"];

asm["RL_rebChar"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebChar.apply(null, arguments);
};

var real__RL_rebInteger = asm["RL_rebInteger"];

asm["RL_rebInteger"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebInteger.apply(null, arguments);
};

var real__RL_rebDecimal = asm["RL_rebDecimal"];

asm["RL_rebDecimal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebDecimal.apply(null, arguments);
};

var real__RL_rebSizedBinary = asm["RL_rebSizedBinary"];

asm["RL_rebSizedBinary"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebSizedBinary.apply(null, arguments);
};

var real__RL_rebUninitializedBinary_internal = asm["RL_rebUninitializedBinary_internal"];

asm["RL_rebUninitializedBinary_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebUninitializedBinary_internal.apply(null, arguments);
};

var real__RL_rebBinaryHead_internal = asm["RL_rebBinaryHead_internal"];

asm["RL_rebBinaryHead_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebBinaryHead_internal.apply(null, arguments);
};

var real__RL_rebBinaryAt_internal = asm["RL_rebBinaryAt_internal"];

asm["RL_rebBinaryAt_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebBinaryAt_internal.apply(null, arguments);
};

var real__RL_rebBinarySizeAt_internal = asm["RL_rebBinarySizeAt_internal"];

asm["RL_rebBinarySizeAt_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebBinarySizeAt_internal.apply(null, arguments);
};

var real__RL_rebSizedText = asm["RL_rebSizedText"];

asm["RL_rebSizedText"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebSizedText.apply(null, arguments);
};

var real__RL_rebText = asm["RL_rebText"];

asm["RL_rebText"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebText.apply(null, arguments);
};

var real__RL_rebLengthedTextWide = asm["RL_rebLengthedTextWide"];

asm["RL_rebLengthedTextWide"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebLengthedTextWide.apply(null, arguments);
};

var real__RL_rebTextWide = asm["RL_rebTextWide"];

asm["RL_rebTextWide"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebTextWide.apply(null, arguments);
};

var real__RL_rebHandle = asm["RL_rebHandle"];

asm["RL_rebHandle"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebHandle.apply(null, arguments);
};

var real__RL_rebArgR = asm["RL_rebArgR"];

asm["RL_rebArgR"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebArgR.apply(null, arguments);
};

var real__RL_rebArg = asm["RL_rebArg"];

asm["RL_rebArg"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebArg.apply(null, arguments);
};

var real__RL_rebValue = asm["RL_rebValue"];

asm["RL_rebValue"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebValue.apply(null, arguments);
};

var real__RL_rebRelease = asm["RL_rebRelease"];

asm["RL_rebRelease"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebRelease.apply(null, arguments);
};

var real__RL_rebQuote = asm["RL_rebQuote"];

asm["RL_rebQuote"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebQuote.apply(null, arguments);
};

var real__RL_rebElide = asm["RL_rebElide"];

asm["RL_rebElide"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebElide.apply(null, arguments);
};

var real__RL_rebDid = asm["RL_rebDid"];

asm["RL_rebDid"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebDid.apply(null, arguments);
};

var real__RL_rebNot = asm["RL_rebNot"];

asm["RL_rebNot"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebNot.apply(null, arguments);
};

var real__RL_rebUnbox = asm["RL_rebUnbox"];

asm["RL_rebUnbox"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebUnbox.apply(null, arguments);
};

var real__RL_rebUnboxInteger = asm["RL_rebUnboxInteger"];

asm["RL_rebUnboxInteger"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebUnboxInteger.apply(null, arguments);
};

var real__RL_rebUnboxDecimal = asm["RL_rebUnboxDecimal"];

asm["RL_rebUnboxDecimal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebUnboxDecimal.apply(null, arguments);
};

var real__RL_rebUnboxChar = asm["RL_rebUnboxChar"];

asm["RL_rebUnboxChar"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebUnboxChar.apply(null, arguments);
};

var real__RL_rebUnboxHandle = asm["RL_rebUnboxHandle"];

asm["RL_rebUnboxHandle"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebUnboxHandle.apply(null, arguments);
};

var real__RL_rebSpellInto = asm["RL_rebSpellInto"];

asm["RL_rebSpellInto"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebSpellInto.apply(null, arguments);
};

var real__RL_rebSpell = asm["RL_rebSpell"];

asm["RL_rebSpell"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebSpell.apply(null, arguments);
};

var real__RL_rebSpellIntoWide = asm["RL_rebSpellIntoWide"];

asm["RL_rebSpellIntoWide"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebSpellIntoWide.apply(null, arguments);
};

var real__RL_rebSpellWide = asm["RL_rebSpellWide"];

asm["RL_rebSpellWide"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebSpellWide.apply(null, arguments);
};

var real__RL_rebBytesInto = asm["RL_rebBytesInto"];

asm["RL_rebBytesInto"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebBytesInto.apply(null, arguments);
};

var real__RL_rebBytes = asm["RL_rebBytes"];

asm["RL_rebBytes"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebBytes.apply(null, arguments);
};

var real__RL_rebRescue = asm["RL_rebRescue"];

asm["RL_rebRescue"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebRescue.apply(null, arguments);
};

var real__malloc = asm["malloc"];

asm["malloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__malloc.apply(null, arguments);
};

var real__free = asm["free"];

asm["free"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__free.apply(null, arguments);
};

var real__RL_rebRescueWith = asm["RL_rebRescueWith"];

asm["RL_rebRescueWith"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebRescueWith.apply(null, arguments);
};

var real__RL_rebHalt = asm["RL_rebHalt"];

asm["RL_rebHalt"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebHalt.apply(null, arguments);
};

var real__RL_rebQUOTING = asm["RL_rebQUOTING"];

asm["RL_rebQUOTING"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebQUOTING.apply(null, arguments);
};

var real__RL_rebUNQUOTING = asm["RL_rebUNQUOTING"];

asm["RL_rebUNQUOTING"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebUNQUOTING.apply(null, arguments);
};

var real__RL_rebRELEASING = asm["RL_rebRELEASING"];

asm["RL_rebRELEASING"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebRELEASING.apply(null, arguments);
};

var real__RL_rebManage = asm["RL_rebManage"];

asm["RL_rebManage"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebManage.apply(null, arguments);
};

var real__RL_rebUnmanage = asm["RL_rebUnmanage"];

asm["RL_rebUnmanage"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebUnmanage.apply(null, arguments);
};

var real__RL_rebZdeflateAlloc = asm["RL_rebZdeflateAlloc"];

asm["RL_rebZdeflateAlloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebZdeflateAlloc.apply(null, arguments);
};

var real__RL_rebZinflateAlloc = asm["RL_rebZinflateAlloc"];

asm["RL_rebZinflateAlloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebZinflateAlloc.apply(null, arguments);
};

var real__RL_rebError_OS = asm["RL_rebError_OS"];

asm["RL_rebError_OS"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebError_OS.apply(null, arguments);
};

var real__fflush = asm["fflush"];

asm["fflush"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__fflush.apply(null, arguments);
};

var real____errno_location = asm["__errno_location"];

asm["__errno_location"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real____errno_location.apply(null, arguments);
};

var real__RL_rebPromise = asm["RL_rebPromise"];

asm["RL_rebPromise"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebPromise.apply(null, arguments);
};

var real__RL_rebSignalResolveNative_internal = asm["RL_rebSignalResolveNative_internal"];

asm["RL_rebSignalResolveNative_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebSignalResolveNative_internal.apply(null, arguments);
};

var real__RL_rebSignalRejectNative_internal = asm["RL_rebSignalRejectNative_internal"];

asm["RL_rebSignalRejectNative_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebSignalRejectNative_internal.apply(null, arguments);
};

var real__RL_rebTakeAwaitLock_internal = asm["RL_rebTakeAwaitLock_internal"];

asm["RL_rebTakeAwaitLock_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__RL_rebTakeAwaitLock_internal.apply(null, arguments);
};

var real__emscripten_get_global_libc = asm["emscripten_get_global_libc"];

asm["emscripten_get_global_libc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_get_global_libc.apply(null, arguments);
};

var real____em_js__initPthreadsJS = asm["__em_js__initPthreadsJS"];

asm["__em_js__initPthreadsJS"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real____em_js__initPthreadsJS.apply(null, arguments);
};

var real____emscripten_pthread_data_constructor = asm["__emscripten_pthread_data_constructor"];

asm["__emscripten_pthread_data_constructor"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real____emscripten_pthread_data_constructor.apply(null, arguments);
};

var real__setThrew = asm["setThrew"];

asm["setThrew"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__setThrew.apply(null, arguments);
};

var real__realloc = asm["realloc"];

asm["realloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__realloc.apply(null, arguments);
};

var real__memalign = asm["memalign"];

asm["memalign"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__memalign.apply(null, arguments);
};

var real__emscripten_main_browser_thread_id = asm["emscripten_main_browser_thread_id"];

asm["emscripten_main_browser_thread_id"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_main_browser_thread_id.apply(null, arguments);
};

var real____pthread_tsd_run_dtors = asm["__pthread_tsd_run_dtors"];

asm["__pthread_tsd_run_dtors"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real____pthread_tsd_run_dtors.apply(null, arguments);
};

var real__emscripten_main_thread_process_queued_calls = asm["emscripten_main_thread_process_queued_calls"];

asm["emscripten_main_thread_process_queued_calls"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_main_thread_process_queued_calls.apply(null, arguments);
};

var real__emscripten_current_thread_process_queued_calls = asm["emscripten_current_thread_process_queued_calls"];

asm["emscripten_current_thread_process_queued_calls"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_current_thread_process_queued_calls.apply(null, arguments);
};

var real__emscripten_register_main_browser_thread_id = asm["emscripten_register_main_browser_thread_id"];

asm["emscripten_register_main_browser_thread_id"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_register_main_browser_thread_id.apply(null, arguments);
};

var real__emscripten_async_run_in_main_thread = asm["emscripten_async_run_in_main_thread"];

asm["emscripten_async_run_in_main_thread"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_async_run_in_main_thread.apply(null, arguments);
};

var real__emscripten_sync_run_in_main_thread = asm["emscripten_sync_run_in_main_thread"];

asm["emscripten_sync_run_in_main_thread"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_sync_run_in_main_thread.apply(null, arguments);
};

var real__emscripten_sync_run_in_main_thread_0 = asm["emscripten_sync_run_in_main_thread_0"];

asm["emscripten_sync_run_in_main_thread_0"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_sync_run_in_main_thread_0.apply(null, arguments);
};

var real__emscripten_sync_run_in_main_thread_1 = asm["emscripten_sync_run_in_main_thread_1"];

asm["emscripten_sync_run_in_main_thread_1"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_sync_run_in_main_thread_1.apply(null, arguments);
};

var real__emscripten_sync_run_in_main_thread_2 = asm["emscripten_sync_run_in_main_thread_2"];

asm["emscripten_sync_run_in_main_thread_2"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_sync_run_in_main_thread_2.apply(null, arguments);
};

var real__emscripten_sync_run_in_main_thread_xprintf_varargs = asm["emscripten_sync_run_in_main_thread_xprintf_varargs"];

asm["emscripten_sync_run_in_main_thread_xprintf_varargs"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_sync_run_in_main_thread_xprintf_varargs.apply(null, arguments);
};

var real__emscripten_sync_run_in_main_thread_3 = asm["emscripten_sync_run_in_main_thread_3"];

asm["emscripten_sync_run_in_main_thread_3"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_sync_run_in_main_thread_3.apply(null, arguments);
};

var real__emscripten_sync_run_in_main_thread_4 = asm["emscripten_sync_run_in_main_thread_4"];

asm["emscripten_sync_run_in_main_thread_4"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_sync_run_in_main_thread_4.apply(null, arguments);
};

var real__emscripten_sync_run_in_main_thread_5 = asm["emscripten_sync_run_in_main_thread_5"];

asm["emscripten_sync_run_in_main_thread_5"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_sync_run_in_main_thread_5.apply(null, arguments);
};

var real__emscripten_sync_run_in_main_thread_6 = asm["emscripten_sync_run_in_main_thread_6"];

asm["emscripten_sync_run_in_main_thread_6"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_sync_run_in_main_thread_6.apply(null, arguments);
};

var real__emscripten_sync_run_in_main_thread_7 = asm["emscripten_sync_run_in_main_thread_7"];

asm["emscripten_sync_run_in_main_thread_7"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_sync_run_in_main_thread_7.apply(null, arguments);
};

var real__emscripten_run_in_main_runtime_thread_js = asm["emscripten_run_in_main_runtime_thread_js"];

asm["emscripten_run_in_main_runtime_thread_js"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_run_in_main_runtime_thread_js.apply(null, arguments);
};

var real__emscripten_async_queue_on_thread_ = asm["emscripten_async_queue_on_thread_"];

asm["emscripten_async_queue_on_thread_"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_async_queue_on_thread_.apply(null, arguments);
};

var real__emscripten_tls_init = asm["emscripten_tls_init"];

asm["emscripten_tls_init"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real__emscripten_tls_init.apply(null, arguments);
};

var real_dynCall_vi = asm["dynCall_vi"];

asm["dynCall_vi"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_dynCall_vi.apply(null, arguments);
};

var real_dynCall_vii = asm["dynCall_vii"];

asm["dynCall_vii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_dynCall_vii.apply(null, arguments);
};

var real_dynCall_i = asm["dynCall_i"];

asm["dynCall_i"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_dynCall_i.apply(null, arguments);
};

var real_dynCall_ii = asm["dynCall_ii"];

asm["dynCall_ii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_dynCall_ii.apply(null, arguments);
};

var real_dynCall_iii = asm["dynCall_iii"];

asm["dynCall_iii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_dynCall_iii.apply(null, arguments);
};

var real____set_stack_limit = asm["__set_stack_limit"];

asm["__set_stack_limit"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real____set_stack_limit.apply(null, arguments);
};

var real_stackSave = asm["stackSave"];

asm["stackSave"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_stackSave.apply(null, arguments);
};

var real_stackAlloc = asm["stackAlloc"];

asm["stackAlloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_stackAlloc.apply(null, arguments);
};

var real_stackRestore = asm["stackRestore"];

asm["stackRestore"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_stackRestore.apply(null, arguments);
};

var real___growWasmMemory = asm["__growWasmMemory"];

asm["__growWasmMemory"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real___growWasmMemory.apply(null, arguments);
};

var real_dynCall_iiii = asm["dynCall_iiii"];

asm["dynCall_iiii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_dynCall_iiii.apply(null, arguments);
};

var real_dynCall_viii = asm["dynCall_viii"];

asm["dynCall_viii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_dynCall_viii.apply(null, arguments);
};

var real_dynCall_iiiii = asm["dynCall_iiiii"];

asm["dynCall_iiiii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_dynCall_iiiii.apply(null, arguments);
};

var real_dynCall_jiji = asm["dynCall_jiji"];

asm["dynCall_jiji"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_dynCall_jiji.apply(null, arguments);
};

var real_dynCall_iidiiii = asm["dynCall_iidiiii"];

asm["dynCall_iidiiii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return real_dynCall_iidiiii.apply(null, arguments);
};

Module["asm"] = asm;

var ___wasm_call_ctors = Module["___wasm_call_ctors"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["__wasm_call_ctors"].apply(null, arguments);
};

var _RL_rebEnterApi_internal = Module["_RL_rebEnterApi_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebEnterApi_internal"].apply(null, arguments);
};

var _RL_rebMalloc = Module["_RL_rebMalloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebMalloc"].apply(null, arguments);
};

var _RL_rebRealloc = Module["_RL_rebRealloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebRealloc"].apply(null, arguments);
};

var _RL_rebFree = Module["_RL_rebFree"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebFree"].apply(null, arguments);
};

var _RL_rebJumps = Module["_RL_rebJumps"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebJumps"].apply(null, arguments);
};

var _RL_rebRepossess = Module["_RL_rebRepossess"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebRepossess"].apply(null, arguments);
};

var _RL_rebStartup = Module["_RL_rebStartup"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebStartup"].apply(null, arguments);
};

var _RL_rebShutdown = Module["_RL_rebShutdown"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebShutdown"].apply(null, arguments);
};

var _RL_rebTick = Module["_RL_rebTick"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebTick"].apply(null, arguments);
};

var _RL_rebVoid = Module["_RL_rebVoid"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebVoid"].apply(null, arguments);
};

var _RL_rebBlank = Module["_RL_rebBlank"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebBlank"].apply(null, arguments);
};

var _RL_rebLogic = Module["_RL_rebLogic"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebLogic"].apply(null, arguments);
};

var _RL_rebChar = Module["_RL_rebChar"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebChar"].apply(null, arguments);
};

var _RL_rebInteger = Module["_RL_rebInteger"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebInteger"].apply(null, arguments);
};

var _RL_rebDecimal = Module["_RL_rebDecimal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebDecimal"].apply(null, arguments);
};

var _RL_rebSizedBinary = Module["_RL_rebSizedBinary"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebSizedBinary"].apply(null, arguments);
};

var _RL_rebUninitializedBinary_internal = Module["_RL_rebUninitializedBinary_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebUninitializedBinary_internal"].apply(null, arguments);
};

var _RL_rebBinaryHead_internal = Module["_RL_rebBinaryHead_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebBinaryHead_internal"].apply(null, arguments);
};

var _RL_rebBinaryAt_internal = Module["_RL_rebBinaryAt_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebBinaryAt_internal"].apply(null, arguments);
};

var _RL_rebBinarySizeAt_internal = Module["_RL_rebBinarySizeAt_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebBinarySizeAt_internal"].apply(null, arguments);
};

var _RL_rebSizedText = Module["_RL_rebSizedText"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebSizedText"].apply(null, arguments);
};

var _RL_rebText = Module["_RL_rebText"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebText"].apply(null, arguments);
};

var _RL_rebLengthedTextWide = Module["_RL_rebLengthedTextWide"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebLengthedTextWide"].apply(null, arguments);
};

var _RL_rebTextWide = Module["_RL_rebTextWide"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebTextWide"].apply(null, arguments);
};

var _RL_rebHandle = Module["_RL_rebHandle"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebHandle"].apply(null, arguments);
};

var _RL_rebArgR = Module["_RL_rebArgR"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebArgR"].apply(null, arguments);
};

var _RL_rebArg = Module["_RL_rebArg"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebArg"].apply(null, arguments);
};

var _RL_rebValue = Module["_RL_rebValue"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebValue"].apply(null, arguments);
};

var _RL_rebRelease = Module["_RL_rebRelease"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebRelease"].apply(null, arguments);
};

var _RL_rebQuote = Module["_RL_rebQuote"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebQuote"].apply(null, arguments);
};

var _RL_rebElide = Module["_RL_rebElide"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebElide"].apply(null, arguments);
};

var _RL_rebDid = Module["_RL_rebDid"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebDid"].apply(null, arguments);
};

var _RL_rebNot = Module["_RL_rebNot"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebNot"].apply(null, arguments);
};

var _RL_rebUnbox = Module["_RL_rebUnbox"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebUnbox"].apply(null, arguments);
};

var _RL_rebUnboxInteger = Module["_RL_rebUnboxInteger"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebUnboxInteger"].apply(null, arguments);
};

var _RL_rebUnboxDecimal = Module["_RL_rebUnboxDecimal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebUnboxDecimal"].apply(null, arguments);
};

var _RL_rebUnboxChar = Module["_RL_rebUnboxChar"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebUnboxChar"].apply(null, arguments);
};

var _RL_rebUnboxHandle = Module["_RL_rebUnboxHandle"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebUnboxHandle"].apply(null, arguments);
};

var _RL_rebSpellInto = Module["_RL_rebSpellInto"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebSpellInto"].apply(null, arguments);
};

var _RL_rebSpell = Module["_RL_rebSpell"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebSpell"].apply(null, arguments);
};

var _RL_rebSpellIntoWide = Module["_RL_rebSpellIntoWide"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebSpellIntoWide"].apply(null, arguments);
};

var _RL_rebSpellWide = Module["_RL_rebSpellWide"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebSpellWide"].apply(null, arguments);
};

var _RL_rebBytesInto = Module["_RL_rebBytesInto"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebBytesInto"].apply(null, arguments);
};

var _RL_rebBytes = Module["_RL_rebBytes"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebBytes"].apply(null, arguments);
};

var _RL_rebRescue = Module["_RL_rebRescue"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebRescue"].apply(null, arguments);
};

var _malloc = Module["_malloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["malloc"].apply(null, arguments);
};

var _free = Module["_free"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["free"].apply(null, arguments);
};

var _RL_rebRescueWith = Module["_RL_rebRescueWith"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebRescueWith"].apply(null, arguments);
};

var _RL_rebHalt = Module["_RL_rebHalt"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebHalt"].apply(null, arguments);
};

var _RL_rebQUOTING = Module["_RL_rebQUOTING"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebQUOTING"].apply(null, arguments);
};

var _RL_rebUNQUOTING = Module["_RL_rebUNQUOTING"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebUNQUOTING"].apply(null, arguments);
};

var _RL_rebRELEASING = Module["_RL_rebRELEASING"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebRELEASING"].apply(null, arguments);
};

var _RL_rebManage = Module["_RL_rebManage"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebManage"].apply(null, arguments);
};

var _RL_rebUnmanage = Module["_RL_rebUnmanage"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebUnmanage"].apply(null, arguments);
};

var _RL_rebZdeflateAlloc = Module["_RL_rebZdeflateAlloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebZdeflateAlloc"].apply(null, arguments);
};

var _RL_rebZinflateAlloc = Module["_RL_rebZinflateAlloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebZinflateAlloc"].apply(null, arguments);
};

var _RL_rebError_OS = Module["_RL_rebError_OS"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebError_OS"].apply(null, arguments);
};

var _fflush = Module["_fflush"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["fflush"].apply(null, arguments);
};

var ___errno_location = Module["___errno_location"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["__errno_location"].apply(null, arguments);
};

var _RL_rebPromise = Module["_RL_rebPromise"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebPromise"].apply(null, arguments);
};

var _RL_rebSignalResolveNative_internal = Module["_RL_rebSignalResolveNative_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebSignalResolveNative_internal"].apply(null, arguments);
};

var _RL_rebSignalRejectNative_internal = Module["_RL_rebSignalRejectNative_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebSignalRejectNative_internal"].apply(null, arguments);
};

var _RL_rebTakeAwaitLock_internal = Module["_RL_rebTakeAwaitLock_internal"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["RL_rebTakeAwaitLock_internal"].apply(null, arguments);
};

var _emscripten_get_global_libc = Module["_emscripten_get_global_libc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_get_global_libc"].apply(null, arguments);
};

var ___em_js__initPthreadsJS = Module["___em_js__initPthreadsJS"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["__em_js__initPthreadsJS"].apply(null, arguments);
};

var ___emscripten_pthread_data_constructor = Module["___emscripten_pthread_data_constructor"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["__emscripten_pthread_data_constructor"].apply(null, arguments);
};

var _setThrew = Module["_setThrew"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["setThrew"].apply(null, arguments);
};

var _realloc = Module["_realloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["realloc"].apply(null, arguments);
};

var _memalign = Module["_memalign"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["memalign"].apply(null, arguments);
};

var _emscripten_main_browser_thread_id = Module["_emscripten_main_browser_thread_id"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_main_browser_thread_id"].apply(null, arguments);
};

var ___pthread_tsd_run_dtors = Module["___pthread_tsd_run_dtors"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["__pthread_tsd_run_dtors"].apply(null, arguments);
};

var _emscripten_main_thread_process_queued_calls = Module["_emscripten_main_thread_process_queued_calls"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_main_thread_process_queued_calls"].apply(null, arguments);
};

var _emscripten_current_thread_process_queued_calls = Module["_emscripten_current_thread_process_queued_calls"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_current_thread_process_queued_calls"].apply(null, arguments);
};

var _emscripten_register_main_browser_thread_id = Module["_emscripten_register_main_browser_thread_id"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_register_main_browser_thread_id"].apply(null, arguments);
};

var _emscripten_async_run_in_main_thread = Module["_emscripten_async_run_in_main_thread"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_async_run_in_main_thread"].apply(null, arguments);
};

var _emscripten_sync_run_in_main_thread = Module["_emscripten_sync_run_in_main_thread"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_sync_run_in_main_thread"].apply(null, arguments);
};

var _emscripten_sync_run_in_main_thread_0 = Module["_emscripten_sync_run_in_main_thread_0"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_sync_run_in_main_thread_0"].apply(null, arguments);
};

var _emscripten_sync_run_in_main_thread_1 = Module["_emscripten_sync_run_in_main_thread_1"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_sync_run_in_main_thread_1"].apply(null, arguments);
};

var _emscripten_sync_run_in_main_thread_2 = Module["_emscripten_sync_run_in_main_thread_2"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_sync_run_in_main_thread_2"].apply(null, arguments);
};

var _emscripten_sync_run_in_main_thread_xprintf_varargs = Module["_emscripten_sync_run_in_main_thread_xprintf_varargs"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_sync_run_in_main_thread_xprintf_varargs"].apply(null, arguments);
};

var _emscripten_sync_run_in_main_thread_3 = Module["_emscripten_sync_run_in_main_thread_3"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_sync_run_in_main_thread_3"].apply(null, arguments);
};

var _emscripten_sync_run_in_main_thread_4 = Module["_emscripten_sync_run_in_main_thread_4"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_sync_run_in_main_thread_4"].apply(null, arguments);
};

var _emscripten_sync_run_in_main_thread_5 = Module["_emscripten_sync_run_in_main_thread_5"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_sync_run_in_main_thread_5"].apply(null, arguments);
};

var _emscripten_sync_run_in_main_thread_6 = Module["_emscripten_sync_run_in_main_thread_6"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_sync_run_in_main_thread_6"].apply(null, arguments);
};

var _emscripten_sync_run_in_main_thread_7 = Module["_emscripten_sync_run_in_main_thread_7"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_sync_run_in_main_thread_7"].apply(null, arguments);
};

var _emscripten_run_in_main_runtime_thread_js = Module["_emscripten_run_in_main_runtime_thread_js"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_run_in_main_runtime_thread_js"].apply(null, arguments);
};

var _emscripten_async_queue_on_thread_ = Module["_emscripten_async_queue_on_thread_"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_async_queue_on_thread_"].apply(null, arguments);
};

var _emscripten_tls_init = Module["_emscripten_tls_init"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["emscripten_tls_init"].apply(null, arguments);
};

var dynCall_vi = Module["dynCall_vi"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["dynCall_vi"].apply(null, arguments);
};

var dynCall_vii = Module["dynCall_vii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["dynCall_vii"].apply(null, arguments);
};

var dynCall_i = Module["dynCall_i"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["dynCall_i"].apply(null, arguments);
};

var dynCall_ii = Module["dynCall_ii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["dynCall_ii"].apply(null, arguments);
};

var dynCall_iii = Module["dynCall_iii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["dynCall_iii"].apply(null, arguments);
};

var ___set_stack_limit = Module["___set_stack_limit"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["__set_stack_limit"].apply(null, arguments);
};

var stackSave = Module["stackSave"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["stackSave"].apply(null, arguments);
};

var stackAlloc = Module["stackAlloc"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["stackAlloc"].apply(null, arguments);
};

var stackRestore = Module["stackRestore"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["stackRestore"].apply(null, arguments);
};

var __growWasmMemory = Module["__growWasmMemory"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["__growWasmMemory"].apply(null, arguments);
};

var dynCall_iiii = Module["dynCall_iiii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["dynCall_iiii"].apply(null, arguments);
};

var dynCall_viii = Module["dynCall_viii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["dynCall_viii"].apply(null, arguments);
};

var dynCall_iiiii = Module["dynCall_iiiii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["dynCall_iiiii"].apply(null, arguments);
};

var dynCall_jiji = Module["dynCall_jiji"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["dynCall_jiji"].apply(null, arguments);
};

var dynCall_iidiiii = Module["dynCall_iidiiii"] = function() {
 assert(runtimeInitialized, "you need to wait for the runtime to be ready (e.g. wait for main() to be called)");
 assert(!runtimeExited, "the runtime was exited (use NO_EXIT_RUNTIME to keep it alive after main() exits)");
 return Module["asm"]["dynCall_iidiiii"].apply(null, arguments);
};

function invoke_vi(index, a1) {
 var sp = stackSave();
 try {
  dynCall_vi(index, a1);
 } catch (e) {
  stackRestore(sp);
  if (e !== e + 0 && e !== "longjmp") throw e;
  _setThrew(1, 0);
 }
}

function invoke_vii(index, a1, a2) {
 var sp = stackSave();
 try {
  dynCall_vii(index, a1, a2);
 } catch (e) {
  stackRestore(sp);
  if (e !== e + 0 && e !== "longjmp") throw e;
  _setThrew(1, 0);
 }
}

function invoke_ii(index, a1) {
 var sp = stackSave();
 try {
  return dynCall_ii(index, a1);
 } catch (e) {
  stackRestore(sp);
  if (e !== e + 0 && e !== "longjmp") throw e;
  _setThrew(1, 0);
 }
}

function invoke_i(index) {
 var sp = stackSave();
 try {
  return dynCall_i(index);
 } catch (e) {
  stackRestore(sp);
  if (e !== e + 0 && e !== "longjmp") throw e;
  _setThrew(1, 0);
 }
}

function invoke_iii(index, a1, a2) {
 var sp = stackSave();
 try {
  return dynCall_iii(index, a1, a2);
 } catch (e) {
  stackRestore(sp);
  if (e !== e + 0 && e !== "longjmp") throw e;
  _setThrew(1, 0);
 }
}

Module["asm"] = asm;

if (!Object.getOwnPropertyDescriptor(Module, "intArrayFromString")) Module["intArrayFromString"] = function() {
 abort("'intArrayFromString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "intArrayToString")) Module["intArrayToString"] = function() {
 abort("'intArrayToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "ccall")) Module["ccall"] = function() {
 abort("'ccall' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "cwrap")) Module["cwrap"] = function() {
 abort("'cwrap' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "setValue")) Module["setValue"] = function() {
 abort("'setValue' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "getValue")) Module["getValue"] = function() {
 abort("'getValue' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "allocate")) Module["allocate"] = function() {
 abort("'allocate' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "getMemory")) Module["getMemory"] = function() {
 abort("'getMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "AsciiToString")) Module["AsciiToString"] = function() {
 abort("'AsciiToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "stringToAscii")) Module["stringToAscii"] = function() {
 abort("'stringToAscii' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "UTF8ArrayToString")) Module["UTF8ArrayToString"] = function() {
 abort("'UTF8ArrayToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "UTF8ToString")) Module["UTF8ToString"] = function() {
 abort("'UTF8ToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "stringToUTF8Array")) Module["stringToUTF8Array"] = function() {
 abort("'stringToUTF8Array' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "stringToUTF8")) Module["stringToUTF8"] = function() {
 abort("'stringToUTF8' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "lengthBytesUTF8")) Module["lengthBytesUTF8"] = function() {
 abort("'lengthBytesUTF8' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "UTF16ToString")) Module["UTF16ToString"] = function() {
 abort("'UTF16ToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "stringToUTF16")) Module["stringToUTF16"] = function() {
 abort("'stringToUTF16' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "lengthBytesUTF16")) Module["lengthBytesUTF16"] = function() {
 abort("'lengthBytesUTF16' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "UTF32ToString")) Module["UTF32ToString"] = function() {
 abort("'UTF32ToString' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "stringToUTF32")) Module["stringToUTF32"] = function() {
 abort("'stringToUTF32' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "lengthBytesUTF32")) Module["lengthBytesUTF32"] = function() {
 abort("'lengthBytesUTF32' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

Module["allocateUTF8"] = allocateUTF8;

if (!Object.getOwnPropertyDescriptor(Module, "stackTrace")) Module["stackTrace"] = function() {
 abort("'stackTrace' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "addOnPreRun")) Module["addOnPreRun"] = function() {
 abort("'addOnPreRun' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "addOnInit")) Module["addOnInit"] = function() {
 abort("'addOnInit' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "addOnPreMain")) Module["addOnPreMain"] = function() {
 abort("'addOnPreMain' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "addOnExit")) Module["addOnExit"] = function() {
 abort("'addOnExit' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "addOnPostRun")) Module["addOnPostRun"] = function() {
 abort("'addOnPostRun' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "writeStringToMemory")) Module["writeStringToMemory"] = function() {
 abort("'writeStringToMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "writeArrayToMemory")) Module["writeArrayToMemory"] = function() {
 abort("'writeArrayToMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "writeAsciiToMemory")) Module["writeAsciiToMemory"] = function() {
 abort("'writeAsciiToMemory' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "addRunDependency")) Module["addRunDependency"] = function() {
 abort("'addRunDependency' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "removeRunDependency")) Module["removeRunDependency"] = function() {
 abort("'removeRunDependency' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "ENV")) Module["ENV"] = function() {
 abort("'ENV' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "FS")) Module["FS"] = function() {
 abort("'FS' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "FS_createFolder")) Module["FS_createFolder"] = function() {
 abort("'FS_createFolder' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "FS_createPath")) Module["FS_createPath"] = function() {
 abort("'FS_createPath' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "FS_createDataFile")) Module["FS_createDataFile"] = function() {
 abort("'FS_createDataFile' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "FS_createPreloadedFile")) Module["FS_createPreloadedFile"] = function() {
 abort("'FS_createPreloadedFile' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "FS_createLazyFile")) Module["FS_createLazyFile"] = function() {
 abort("'FS_createLazyFile' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "FS_createLink")) Module["FS_createLink"] = function() {
 abort("'FS_createLink' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "FS_createDevice")) Module["FS_createDevice"] = function() {
 abort("'FS_createDevice' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "FS_unlink")) Module["FS_unlink"] = function() {
 abort("'FS_unlink' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
};

if (!Object.getOwnPropertyDescriptor(Module, "GL")) Module["GL"] = function() {
 abort("'GL' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "dynamicAlloc")) Module["dynamicAlloc"] = function() {
 abort("'dynamicAlloc' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "loadDynamicLibrary")) Module["loadDynamicLibrary"] = function() {
 abort("'loadDynamicLibrary' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "loadWebAssemblyModule")) Module["loadWebAssemblyModule"] = function() {
 abort("'loadWebAssemblyModule' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "getLEB")) Module["getLEB"] = function() {
 abort("'getLEB' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "getFunctionTables")) Module["getFunctionTables"] = function() {
 abort("'getFunctionTables' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "alignFunctionTables")) Module["alignFunctionTables"] = function() {
 abort("'alignFunctionTables' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "registerFunctions")) Module["registerFunctions"] = function() {
 abort("'registerFunctions' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "addFunction")) Module["addFunction"] = function() {
 abort("'addFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "removeFunction")) Module["removeFunction"] = function() {
 abort("'removeFunction' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "getFuncWrapper")) Module["getFuncWrapper"] = function() {
 abort("'getFuncWrapper' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "prettyPrint")) Module["prettyPrint"] = function() {
 abort("'prettyPrint' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "makeBigInt")) Module["makeBigInt"] = function() {
 abort("'makeBigInt' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "dynCall")) Module["dynCall"] = function() {
 abort("'dynCall' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "getCompilerSetting")) Module["getCompilerSetting"] = function() {
 abort("'getCompilerSetting' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "print")) Module["print"] = function() {
 abort("'print' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "printErr")) Module["printErr"] = function() {
 abort("'printErr' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "getTempRet0")) Module["getTempRet0"] = function() {
 abort("'getTempRet0' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "setTempRet0")) Module["setTempRet0"] = function() {
 abort("'setTempRet0' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "callMain")) Module["callMain"] = function() {
 abort("'callMain' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "abort")) Module["abort"] = function() {
 abort("'abort' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "warnOnce")) Module["warnOnce"] = function() {
 abort("'warnOnce' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "stackSave")) Module["stackSave"] = function() {
 abort("'stackSave' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "stackRestore")) Module["stackRestore"] = function() {
 abort("'stackRestore' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

if (!Object.getOwnPropertyDescriptor(Module, "stackAlloc")) Module["stackAlloc"] = function() {
 abort("'stackAlloc' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
};

Module["establishStackSpace"] = establishStackSpace;

Module["writeStackCookie"] = writeStackCookie;

Module["checkStackCookie"] = checkStackCookie;

Module["abortStackOverflow"] = abortStackOverflow;

Module["PThread"] = PThread;

Module["ExitStatus"] = ExitStatus;

Module["_pthread_self"] = _pthread_self;

Module["wasmMemory"] = wasmMemory;

if (!Object.getOwnPropertyDescriptor(Module, "ALLOC_NORMAL")) Object.defineProperty(Module, "ALLOC_NORMAL", {
 configurable: true,
 get: function() {
  abort("'ALLOC_NORMAL' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
 }
});

if (!Object.getOwnPropertyDescriptor(Module, "ALLOC_STACK")) Object.defineProperty(Module, "ALLOC_STACK", {
 configurable: true,
 get: function() {
  abort("'ALLOC_STACK' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
 }
});

if (!Object.getOwnPropertyDescriptor(Module, "ALLOC_DYNAMIC")) Object.defineProperty(Module, "ALLOC_DYNAMIC", {
 configurable: true,
 get: function() {
  abort("'ALLOC_DYNAMIC' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
 }
});

if (!Object.getOwnPropertyDescriptor(Module, "ALLOC_NONE")) Object.defineProperty(Module, "ALLOC_NONE", {
 configurable: true,
 get: function() {
  abort("'ALLOC_NONE' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ)");
 }
});

if (!Object.getOwnPropertyDescriptor(Module, "calledRun")) Object.defineProperty(Module, "calledRun", {
 configurable: true,
 get: function() {
  abort("'calledRun' was not exported. add it to EXTRA_EXPORTED_RUNTIME_METHODS (see the FAQ). Alternatively, forcing filesystem support (-s FORCE_FILESYSTEM=1) can export this for you");
 }
});

var calledRun;

function ExitStatus(status) {
 this.name = "ExitStatus";
 this.message = "Program terminated with exit(" + status + ")";
 this.status = status;
}

dependenciesFulfilled = function runCaller() {
 if (!calledRun) run();
 if (!calledRun) dependenciesFulfilled = runCaller;
};

function run(args) {
 args = args || arguments_;
 if (runDependencies > 0) {
  return;
 }
 writeStackCookie();
 preRun();
 if (runDependencies > 0) return;
 function doRun() {
  if (calledRun) return;
  calledRun = true;
  if (ABORT) return;
  initRuntime();
  preMain();
  if (Module["onRuntimeInitialized"]) Module["onRuntimeInitialized"]();
  assert(!Module["_main"], 'compiled without a main, but one is present. if you added it from JS, use Module["onRuntimeInitialized"]');
  postRun();
 }
 if (Module["setStatus"]) {
  Module["setStatus"]("Running...");
  setTimeout(function() {
   setTimeout(function() {
    Module["setStatus"]("");
   }, 1);
   doRun();
  }, 1);
 } else {
  doRun();
 }
 checkStackCookie();
}

Module["run"] = run;

function checkUnflushedContent() {
 var print = out;
 var printErr = err;
 var has = false;
 out = err = function(x) {
  has = true;
 };
 try {
  var flush = flush_NO_FILESYSTEM;
  if (flush) flush(0);
 } catch (e) {}
 out = print;
 err = printErr;
 if (has) {
  warnOnce("stdio streams had content in them that was not flushed. you should set EXIT_RUNTIME to 1 (see the FAQ), or make sure to emit a newline when you printf etc.");
  warnOnce("(this may also be due to not including full filesystem support - try building with -s FORCE_FILESYSTEM=1)");
 }
}

function exit(status, implicit) {
 checkUnflushedContent();
 if (implicit && noExitRuntime && status === 0) {
  return;
 }
 if (noExitRuntime) {
  if (!implicit) {
   err("program exited (with status: " + status + "), but EXIT_RUNTIME is not set, so halting execution but not exiting the runtime or preventing further async execution (build with EXIT_RUNTIME=1, if you want a true shutdown)");
  }
 } else {
  PThread.terminateAllThreads();
  ABORT = true;
  EXITSTATUS = status;
  exitRuntime();
  if (Module["onExit"]) Module["onExit"](status);
 }
 quit_(status, new ExitStatus(status));
}

if (Module["preInit"]) {
 if (typeof Module["preInit"] == "function") Module["preInit"] = [ Module["preInit"] ];
 while (Module["preInit"].length > 0) {
  Module["preInit"].pop()();
 }
}

if (!ENVIRONMENT_IS_PTHREAD) noExitRuntime = true;

if (!ENVIRONMENT_IS_PTHREAD) run();

function ccall_tolerant(ident, returnType, argTypes, args, opts) {
 var toC = {
  "string": function(str) {
   var ret = 0;
   if (str !== null && str !== undefined && str !== 0) {
    var len = (str.length << 2) + 1;
    ret = stackAlloc(len);
    stringToUTF8(str, ret, len);
   }
   return ret;
  },
  "array": function(arr) {
   var ret = stackAlloc(arr.length);
   writeArrayToMemory(arr, ret);
   return ret;
  }
 };
 function convertReturnValue(ret) {
  if (returnType === "string") return UTF8ToString(ret);
  if (returnType === "boolean") return Boolean(ret);
  return ret;
 }
 var func = getCFunc(ident);
 var cArgs = [];
 var stack = 0;
 if (args) {
  for (var i = 0; i < args.length; i++) {
   var converter = toC[argTypes[i]];
   if (converter) {
    if (stack === 0) stack = stackSave();
    cArgs[i] = converter(args[i]);
   } else {
    cArgs[i] = args[i];
   }
  }
 }
 var ret = func.apply(null, cArgs);
 ret = convertReturnValue(ret);
 if (stack !== 0) stackRestore(stack);
 return ret;
}

function cwrap_tolerant(ident, returnType, argTypes, opts) {
 return function() {
  return ccall_tolerant(ident, returnType, argTypes, arguments, opts);
 };
}

var reb;

if (typeof module !== "undefined") reb = module.exports; else reb = {};

reb.Malloc = cwrap_tolerant("RL_rebMalloc", "number", [ "number" ]);

reb.Realloc = cwrap_tolerant("RL_rebRealloc", "number", [ "number", "number" ]);

reb.Free = cwrap_tolerant("RL_rebFree", undefined, [ "number" ]);

reb.Repossess = cwrap_tolerant("RL_rebRepossess", "number", [ "number", "number" ]);

reb.Shutdown = cwrap_tolerant("RL_rebShutdown", undefined, [ "Boolean" ]);

reb.Tick = cwrap_tolerant("RL_rebTick", "number", []);

reb.Void = cwrap_tolerant("RL_rebVoid", "number", []);

reb.Blank = cwrap_tolerant("RL_rebBlank", "number", []);

reb.Logic = cwrap_tolerant("RL_rebLogic", "number", [ "Boolean" ]);

reb.Char = cwrap_tolerant("RL_rebChar", "number", [ "number" ]);

reb.Integer = cwrap_tolerant("RL_rebInteger", "number", [ "number" ]);

reb.Decimal = cwrap_tolerant("RL_rebDecimal", "number", [ "number" ]);

reb.SizedBinary = cwrap_tolerant("RL_rebSizedBinary", "number", [ "number", "number" ]);

reb.SizedText = cwrap_tolerant("RL_rebSizedText", "number", [ "string", "number" ]);

reb.Text = cwrap_tolerant("RL_rebText", "number", [ "string" ]);

reb.LengthedTextWide = cwrap_tolerant("RL_rebLengthedTextWide", "number", [ "number", "number" ]);

reb.TextWide = cwrap_tolerant("RL_rebTextWide", "number", [ "number" ]);

reb.Handle = cwrap_tolerant("RL_rebHandle", "number", [ "number", "number", "number" ]);

reb.ArgR_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebArgR(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.ArgR = reb.ArgR_qlevel.bind({
 quotes: 0
});

reb.ArgRQ = reb.ArgR_qlevel.bind({
 quotes: 1
});

reb.Arg_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebArg(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.Arg = reb.Arg_qlevel.bind({
 quotes: 0
});

reb.ArgQ = reb.Arg_qlevel.bind({
 quotes: 1
});

reb.Value_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebValue(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.Value = reb.Value_qlevel.bind({
 quotes: 0
});

reb.ValueQ = reb.Value_qlevel.bind({
 quotes: 1
});

reb.Quote_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebQuote(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.Quote = reb.Quote_qlevel.bind({
 quotes: 0
});

reb.QuoteQ = reb.Quote_qlevel.bind({
 quotes: 1
});

reb.Elide_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebElide(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.Elide = reb.Elide_qlevel.bind({
 quotes: 0
});

reb.ElideQ = reb.Elide_qlevel.bind({
 quotes: 1
});

reb.Jumps_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebJumps(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.Jumps = reb.Jumps_qlevel.bind({
 quotes: 0
});

reb.JumpsQ = reb.Jumps_qlevel.bind({
 quotes: 1
});

reb.Did_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebDid(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.Did = reb.Did_qlevel.bind({
 quotes: 0
});

reb.DidQ = reb.Did_qlevel.bind({
 quotes: 1
});

reb.Not_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebNot(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.Not = reb.Not_qlevel.bind({
 quotes: 0
});

reb.NotQ = reb.Not_qlevel.bind({
 quotes: 1
});

reb.Unbox_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebUnbox(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.Unbox = reb.Unbox_qlevel.bind({
 quotes: 0
});

reb.UnboxQ = reb.Unbox_qlevel.bind({
 quotes: 1
});

reb.UnboxInteger_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebUnboxInteger(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.UnboxInteger = reb.UnboxInteger_qlevel.bind({
 quotes: 0
});

reb.UnboxIntegerQ = reb.UnboxInteger_qlevel.bind({
 quotes: 1
});

reb.UnboxDecimal_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebUnboxDecimal(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.UnboxDecimal = reb.UnboxDecimal_qlevel.bind({
 quotes: 0
});

reb.UnboxDecimalQ = reb.UnboxDecimal_qlevel.bind({
 quotes: 1
});

reb.UnboxChar_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebUnboxChar(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.UnboxChar = reb.UnboxChar_qlevel.bind({
 quotes: 0
});

reb.UnboxCharQ = reb.UnboxChar_qlevel.bind({
 quotes: 1
});

reb.Spell_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebSpell(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 var js_str = UTF8ToString(a);
 reb.Free(a);
 return js_str;
};

reb.Spell = reb.Spell_qlevel.bind({
 quotes: 0
});

reb.SpellQ = reb.Spell_qlevel.bind({
 quotes: 1
});

reb.SpellWide_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebSpellWide(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.SpellWide = reb.SpellWide_qlevel.bind({
 quotes: 0
});

reb.SpellWideQ = reb.SpellWide_qlevel.bind({
 quotes: 1
});

reb.Rescue = cwrap_tolerant("RL_rebRescue", "number", [ "number", "number" ]);

reb.RescueWith = cwrap_tolerant("RL_rebRescueWith", "number", [ "number", "number", "number" ]);

reb.QUOTING_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebQUOTING(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.QUOTING = reb.QUOTING_qlevel.bind({
 quotes: 0
});

reb.QUOTINGQ = reb.QUOTING_qlevel.bind({
 quotes: 1
});

reb.UNQUOTING_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebUNQUOTING(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return a;
};

reb.UNQUOTING = reb.UNQUOTING_qlevel.bind({
 quotes: 0
});

reb.UNQUOTINGQ = reb.UNQUOTING_qlevel.bind({
 quotes: 1
});

reb.RELEASING = cwrap_tolerant("RL_rebRELEASING", "number", [ "number" ]);

reb.Manage = cwrap_tolerant("RL_rebManage", "number", [ "number" ]);

reb.Unmanage = cwrap_tolerant("RL_rebUnmanage", undefined, [ "number" ]);

reb.Release = cwrap_tolerant("RL_rebRelease", undefined, [ "number" ]);

reb.ZdeflateAlloc = cwrap_tolerant("RL_rebZdeflateAlloc", "number", [ "number", "number", "number" ]);

reb.ZinflateAlloc = cwrap_tolerant("RL_rebZinflateAlloc", "number", [ "number", "number", "number", "number" ]);

reb.Error_OS = cwrap_tolerant("RL_rebError_OS", "number", [ "number" ]);

reb.Promise_qlevel = function() {
 _RL_rebEnterApi_internal();
 var argc = arguments.length;
 var stack = stackSave();
 var va = stackAlloc(4 * (argc + 1 + 1));
 var a, i, l, p;
 for (i = 0; i < argc; i++) {
  a = arguments[i];
  switch (typeof a) {
  case "string":
   l = lengthBytesUTF8(a) + 4;
   l = l & ~3;
   p = stackAlloc(l);
   stringToUTF8(a, p, l);
   break;

  case "number":
   p = a;
   break;

  default:
   throw Error("Invalid type!");
  }
  HEAP32[(va >> 2) + i] = p;
 }
 HEAP32[(va >> 2) + argc] = reb.END;
 HEAP32[(va >> 2) + (argc + 1)] = va + 4;
 a = _RL_rebPromise(this.quotes, HEAP32[va >> 2], va + 4 * (argc + 1));
 stackRestore(stack);
 return new Promise(function(resolve, reject) {
  reb.RegisterId_internal(a, [ resolve, reject ]);
 });
};

reb.Promise = reb.Promise_qlevel.bind({
 quotes: 0
});

reb.PromiseQ = reb.Promise_qlevel.bind({
 quotes: 1
});

reb.R = reb.RELEASING;

reb.Q = reb.QUOTING;

reb.U = reb.UNQUOTING;

reb.T = function(utf8) {
 return reb.R(reb.Text(utf8));
};

reb.I = function(int64) {
 return reb.R(reb.Integer(int64));
};

reb.L = function(flag) {
 return reb.R(reb.Logic(flag));
};

reb.V = function() {
 return reb.R(reb.Value.apply(null, arguments));
};

reb.Startup = function() {
 _RL_rebStartup();
 reb.END = _malloc(2);
 setValue(reb.END, -127, "i8");
 setValue(reb.END + 1, 0, "i8");
};

reb.Binary = function(array) {
 let view = null;
 if (array instanceof ArrayBuffer) view = new Int8Array(array); else if (array instanceof Int8Array) view = array; else if (array instanceof Uint8Array) view = array; else throw Error("Unknown array type in reb.Binary " + typeof array);
 let binary = _RL_rebUninitializedBinary_internal(view.length);
 let head = _RL_rebBinaryHead_internal(binary);
 writeArrayToMemory(view, head);
 return binary;
};

reb.Bytes = function(binary) {
 let ptr = _RL_rebBinaryAt_internal(binary);
 let size = _RL_rebBinarySizeAt_internal(binary);
 var view = new Uint8Array(Module.HEAPU8.buffer, ptr, size);
 var buffer = new ArrayBuffer(size);
 new Uint8Array(buffer).set(view);
 return buffer;
};

let RL_JS_NATIVES = {};

let RL_JS_CANCELABLES = new Set();

const RL_JS_ERROR_HALTED = Error("Halted by Escape, reb.Halt(), or HALT");

reb.Cancelable = (promise => {
 let cancel;
 let cancelable = new Promise((resolve, reject) => {
  let wasCanceled = false;
  promise.then(val => {
   if (wasCanceled) {} else {
    resolve(val);
    RL_JS_CANCELABLES.delete(cancelable);
   }
  });
  promise.catch(error => {
   if (wasCanceled) {} else {
    reject(error);
    RL_JS_CANCELABLES.delete(cancelable);
   }
  });
  cancel = function() {
   if (typeof promise.cancel === "function") {}
   wasCanceled = true;
   reject(RL_JS_ERROR_HALTED);
   RL_JS_CANCELABLES.delete(cancelable);
  };
 });
 cancelable.cancel = cancel;
 RL_JS_CANCELABLES.add(cancelable);
 return cancelable;
});

reb.Halt = function() {
 _RL_rebHalt();
 RL_JS_CANCELABLES.forEach(promise => {
  promise.cancel();
 });
};

reb.RegisterId_internal = function(id, fn) {
 if (id in RL_JS_NATIVES) throw Error("Already registered " + id + " in JS_NATIVES table");
 RL_JS_NATIVES[id] = fn;
};

reb.UnregisterId_internal = function(id) {
 if (!(id in RL_JS_NATIVES)) throw Error("Can't delete " + id + " in JS_NATIVES table");
 delete RL_JS_NATIVES[id];
};

reb.RunNative_internal = function(id, frame_id) {
 if (!(id in RL_JS_NATIVES)) throw Error("Can't dispatch " + id + " in JS_NATIVES table");
 let resolver = function(res) {
  if (arguments.length > 1) throw Error("JS-NATIVE's return/resolve() takes 1 argument");
  if (res === undefined) {} else if (res === null) {} else if (typeof res == "function") {
   throw Error("JS-NATIVE resolve no longer needs to take JS functions!" + " Asyncify resolved the Emterpreter issues!" + " Please update your code to what is natural!" + " https://github.com/hostilefork/replpad-js/commit/2797e3c006aa4046a488939a4270bb7ebadee70f");
  } else if (typeof res !== "number") {
   console.log("typeof " + typeof res);
   console.log(res);
   throw Error("JS-NATIVE return/resolve takes REBVAL*, null, undefined");
  }
  RL_JS_NATIVES[frame_id] = res;
  _RL_rebSignalResolveNative_internal(frame_id);
 };
 let rejecter = function(rej) {
  console.log(rej);
  if (arguments.length > 1) throw Error("JS-AWAITER's reject() takes 1 argument");
  if (typeof rej == "number") console.log("Suspicious numeric throw() in JS-AWAITER");
  RL_JS_NATIVES[frame_id] = rej;
  _RL_rebSignalRejectNative_internal(frame_id);
 };
 let native = RL_JS_NATIVES[id];
 if (native.is_awaiter) {
  let promise = reb.Cancelable(native());
  promise.then(resolver).catch(rejecter);
 } else {
  try {
   resolver(native());
  } catch (e) {
   rejecter(e);
  }
 }
};

reb.GetNativeResult_internal = function(frame_id) {
 var result = RL_JS_NATIVES[frame_id];
 reb.UnregisterId_internal(frame_id);
 if (typeof result == "function") result = result();
 if (result === null) return 0;
 if (result === undefined) return reb.Void();
 return result;
};

reb.GetNativeError_internal = function(frame_id) {
 var result = RL_JS_NATIVES[frame_id];
 reb.UnregisterId_internal(frame_id);
 if (result == RL_JS_ERROR_HALTED) return 0;
 return reb.Value("make error!", reb.T(String(result)));
};

reb.ResolvePromise_internal = function(promise_id, rebval) {
 if (!(promise_id in RL_JS_NATIVES)) throw Error("Can't find promise_id " + promise_id + " in JS_NATIVES");
 RL_JS_NATIVES[promise_id][0](rebval);
 reb.UnregisterId_internal(promise_id);
};

reb.RejectPromise_internal = function(promise_id, throw_id) {
 if (!(throw_id in RL_JS_NATIVES)) throw Error("Can't find throw_id " + throw_id + " in JS_NATIVES");
 let error = RL_JS_NATIVES[throw_id];
 reb.UnregisterId_internal(throw_id);
 if (!(promise_id in RL_JS_NATIVES)) throw Error("Can't find promise_id " + promise_id + " in JS_NATIVES");
 RL_JS_NATIVES[promise_id][1](error);
 reb.UnregisterId_internal(promise_id);
};

reb.Box = function(js_value) {
 if (null === js_value) return null;
 switch (typeof js_value) {
 case "undefined":
  return reb.Void();

 case "number":
  return reb.Integer(js_value);

 case "string":
  return reb.Text(js_value);

 default:
  return reb.Void();
 }
};
