Rebol [
    file: %code.reb
    title: "Startup sequence"
    date: 17-Jan-2020
    notes: {if this file is found, then instead of opening up the console this is evaluated instead}
    
]

replpad-hello: js-awaiter [
    {set contents of the browser window with hello}
]{
    replpad.innerHTML = "<h1>Welcome to Rebol WASM</h1><p>We are just using Rebol to print to the screen and then exit</p>"
}
    
replpad-hello
