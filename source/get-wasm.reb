Rebol [
    file: %get-wasm.reb
    title: "Get WASM files"
    date: 17-Jan-2020
    notes: {downloads the latest rebol wasm/js files and saves them as
        libr3.js libr3.wasm libr3.worker.js
        
        which are the names used when serving from your own server
        
        use: rebol get-wasm.reb
        
        should run in the %www/ directory
    }
]

root: https://metaeducation.s3.amazonaws.com/travis-builds/0.16.2/ 
; https://dd498l1ilnrxu.cloudfront.net/travis-builds/0.16.2/

hash: to text! read hashname: join root 'last-deploy.short-hash

print spaced ["Commit Date:" get in info? hashname 'date]

dump hash

; have to change back to local directory since a do changes to the web directory
change-dir system/options/path

if %www/ = last split-path what-dir [
    if not exists? %assets/ [
        mkdir %assets/
    ]
    if not exists? %assets/js [
        mkdir/deep %assets/js
    ]
    if not exists? %assets/js/0.16.2/ [
        mkdir/deep %assets/js/0.16.2/
    ]
] else  [
    print "This needs to run in the %www/ directory so the files are saved in the correct directory"
    halt
]

saveto: %assets/js/0.16.2/

for-each ext [%.js %.wasm %.worker.js][
    filename: to url! unspaced [root "libr3-" hash ext] 
    probe filename
    write to file! unspaced [saveto "libr3" ext] read filename
]